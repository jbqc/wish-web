import { RouteRecordRaw } from "vue-router";
import { BasicLayout, BlankLayout } from "/@/layouts/index"
import { t } from '/@/i18n/index'

/**
 * 演示模块路由
 */
const OrgAsyncRoutes: Array<RouteRecordRaw> = [
    {
        path: '/org',
        name: 'Org',
        meta: {
            title: t('menu.org.title'),
            icon: 'icon-team',
            menuId: 'org'
        },
        component: BasicLayout,
        redirect: '/org/user',
        children: [
            //组织与用户
            {
                path: 'user',
                name: 'OrgUser',
                meta: {
                    title: t('menu.org.orgAndUsers.title'),
                    icon: 'icon-user',
                    menuId: 'org:00'
                },
                component: BlankLayout,
                redirect: '/org/user/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgUserIndex',
                        meta: {
                            title: t('menu.org.orgAndUsers.listTitle'),
                            subTitle: t('menu.org.orgAndUsers.subTitle'),
                            icon: 'icon-user',
                            menuId: 'org:00:01',
                            keepAlive: true //设置页面是否需要使用缓存
                        },
                        component: () => import('/@/views/org/user/index.vue'),
                    },
                    {
                        path: 'add',
                        name: 'OrgUserAdd',
                        meta: {
                            title: t('menu.org.orgAndUsers.addTitle'),
                            subTitle: t('menu.org.orgAndUsers.subTitle'),
                            icon: 'icon-user',
                            hidden: true,
                            menuId: 'org:00:02'
                        },
                        component: () => import('/@/views/org/user/form.vue'),
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrgUserEdit',
                        meta: {
                            title: t('menu.org.orgAndUsers.editTitle'),
                            subTitle: t('menu.org.orgAndUsers.subTitle'),
                            icon: 'icon-user',
                            hidden: true,
                            menuId: 'org:00:03'
                        },
                        component: () => import('/@/views/org/user/form.vue'),
                    },
                ]
            },
            //角色与权限
            {
                path: 'role',
                name: 'OrgRole',
                meta: {
                    title: t('menu.org.roleAndAuth.title'),
                    icon: 'icon-safetycertificate',
                    menuId: 'org:01'

                },
                component: BlankLayout,
                redirect: '/org/role/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgRoleIndex',
                        meta: {
                            title: t('menu.org.roleAndAuth.listTitle'),
                            subTitle: t('menu.org.roleAndAuth.subTitle'),
                            icon: 'icon-safetycertificate',
                            menuId: 'org:01:00',
                            keepAlive: true //设置页面是否需要使用缓存
                        },
                        component: () => import('/@/views/org/role/index.vue'),
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrgRoleEdit',
                        meta: {
                            title: t('menu.org.roleAndAuth.editTitle'),
                            subTitle: t('menu.org.roleAndAuth.subTitle'),
                            hidden: true,
                            menuId: 'org:01:01'
                        },
                        component: () => import('/@/views/org/role/form.vue'),
                    },
                ]

            },
            //数据字典
            {
                path: 'dictionary',
                name: 'OrgDictionary',
                meta: {
                    menuId: 'org:02',
                    title: t('menu.org.dictionary.title'),
                    icon: 'icon-read'
                },
                component: BlankLayout,
                redirect: '/org/dictionary/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgDictionaryIndex',
                        meta: {
                            menuId: 'org:02:00',
                            title: t('menu.org.dictionary.listTitle'),
                            subTitle: t('menu.org.dictionary.subTitle'),
                            icon: 'icon-read'
                        },
                        component: () => import('/@/views/org/dictionary-type/index.vue'),
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrgDictionaryEdit',
                        meta: {
                            menuId: 'org:02:01',
                            title: t('menu.org.dictionary.editTitle'),
                            subTitle: t('menu.org.dictionary.subTitle'),
                            hidden: true
                        },
                        component: () => import('/@/views/org/dictionary-type/form.vue'),
                    },
                ]
            },
            //接口管理
            {
                path: 'app-action',
                name: 'OrgAppAction',
                meta: {
                    menuId: 'org:04',
                    title: t('menu.org.appAction.title'),
                    icon: 'icon-retweet'
                },
                component: BlankLayout,
                redirect: '/org/app-action/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgAppActionIndex',
                        meta: {
                            menuId: 'org:04:00',
                            title: t('menu.org.appAction.listTitle'),
                            subTitle: t('menu.org.appAction.subTitle'),
                            icon: 'icon-retweet'
                        },
                        component: () => import('/@/views/org/app-action/index.vue'),
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrgAppActionEdit',
                        meta: {
                            menuId: 'org:04:01',
                            title: t('menu.org.appAction.editTitle'),
                            subTitle: t('menu.org.appAction.subTitle'),
                            hidden: true,
                            icon: 'icon-retweet'
                        },
                        component: () => import('/@/views/org/app-action/form.vue'),
                    },
                ]
            },
            //定时任务管理
            {
                path: 'cron-task',
                name: 'OrgCronTask',
                meta: {
                    menuId: 'org:05',
                    title: t('menu.org.cronTask.title'),
                    icon: 'icon-retweet'
                },
                component: BlankLayout,
                redirect: '/org/cron-task/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgCronTaskIndex',
                        meta: {
                            menuId: 'org:05:00',
                            title: t('menu.org.cronTask.listTitle'),
                            subTitle: t('menu.org.cronTask.subTitle'),
                            icon: 'icon-time-circle'
                        },
                        component: () => import('/@/views/org/cron-task/index.vue'),
                    }
                ]
            },
            //登录日志管理
            {
                path: 'login-log',
                name: 'OrgLoginLog',
                meta: {
                    menuId: 'org:07',
                    title: t('menu.org.loginLog.title'),
                    icon: 'icon-reconciliation'
                },
                component: BlankLayout,
                redirect: '/org/login-log/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgLoginLogIndex',
                        meta: {
                            menuId: 'org:07:00',
                            title: t('menu.org.loginLog.listTitle'),
                            subTitle: t('menu.org.loginLog.subTitle'),
                            icon: 'icon-reconciliation'
                        },
                        component: () => import('/@/views/org/login-log/index.vue'),
                    },
                ]
            },
            //多数据源管理
            {
                path: 'data-source',
                name: 'OrgDataSource',
                meta: {
                    menuId: 'org:08',
                    title: t('menu.org.dataSource.title'),
                    icon: 'icon-database'
                },
                component: BlankLayout,
                redirect: '/org/data-source/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgDataSourceIndex',
                        meta: {
                            menuId: 'org:08:00',
                            title: t('menu.org.dataSource.listTitle'),
                            subTitle: t('menu.org.dataSource.subTitle'),
                            icon: 'icon-database'
                        },
                        component: () => import('/@/views/org/data-source/index.vue'),
                    },
                    {
                        path: 'add',
                        name: 'OrgDataSourceAdd',
                        meta: {
                            title: t('menu.org.dataSource.addTitle'),
                            subTitle: t('menu.org.dataSource.subTitle'),
                            icon: 'icon-user',
                            hidden: true,
                            menuId: 'org:08:02'
                        },
                        component: () => import('/@/views/org/data-source/form.vue'),
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrgDataSourceEdit',
                        meta: {
                            title: t('menu.org.dataSource.editTitle'),
                            subTitle: t('menu.org.dataSource.subTitle'),
                            icon: 'icon-user',
                            hidden: true,
                            menuId: 'org:08:03'
                        },
                        component: () => import('/@/views/org/data-source/form.vue'),
                    },
                ]
            },
            //缓存管理
            {
                path: 'cache',
                name: 'OrgCache',
                meta: {
                    menuId: 'org:09',
                    title: t('menu.org.cache.title'),
                    icon: 'icon-cloud-sync'
                },
                component: BlankLayout,
                redirect: '/org/cache/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgCacheIndex',
                        meta: {
                            menuId: 'org:09:00',
                            title: t('menu.org.cache.listTitle'),
                            subTitle: t('menu.org.cache.subTitle'),
                            icon: 'icon-cloud-sync'
                        },
                        component: () => import('/@/views/org/cache/index.vue'),
                    },
                ]
            },
            // 通知公告
            {
                path: 'notice',
                name: 'OrgNotice',
                meta: {
                    menuId: 'org:10',
                    title: t('menu.org.notice.title'),
                    icon: 'icon-reconciliation'
                },
                component: BlankLayout,
                redirect: '/org/notice/index',
                children: [
                    {
                        path: 'index',
                        name: 'OrgNoticeIndex',
                        meta: {
                            menuId: 'org:10:00',
                            title: t('menu.org.notice.listTitle'),
                            subTitle: t('menu.org.notice.subTitle'),
                            icon: 'icon-reconciliation'
                        },
                        component: () => import('/@/views/org/notice/index.vue'),
                    },
                ]
            },
        ]
    }
]

export default OrgAsyncRoutes;