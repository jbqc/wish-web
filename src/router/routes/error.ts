import { RouteRecordRaw } from "vue-router";
import { BasicLayout } from "/@/layouts/index"
import { t } from '/@/i18n/index'
/**
 * 演示模块路由
 */
const ErrorAsyncRoutes: Array<RouteRecordRaw> = [
    {
        path: '/error',
        name: 'Error',
        meta: {
            menuId: 'error',
            title: t('menu.exception.title'),
            icon: 'icon-error'
        },
        component: BasicLayout,
        redirect: '/error/forbidden',
        children: [
            {
                path: 'forbidden',
                name: 'ErrorForbidden',
                meta: {
                    menuId: 'error:00',
                    title: '403',
                },
                component: () => import('/@/views/error/forbidden.vue')
            },
            {
                path: 'notfound',
                name: 'ErrorNotfound',
                meta: {
                    menuId: 'error:01',
                    title: '404',
                },
                component: () => import('/@/views/error/notfound.vue')
            },
            {
                path: 'exception',
                name: 'ErrorException',
                meta: {
                    menuId: 'error:02',
                    title: '405',
                },
                component: () => import('/@/views/error/exception.vue')
            }
        ]
    }
]

export default ErrorAsyncRoutes;