import { RouteRecordRaw } from 'vue-router'
import DashboardAsyncRoutes from './dashboard'
import ErrorAsyncRoutes from './error'
import AssemblyAsyncRoutes from './assembly'
import OrgAsyncRoutes from './org'

const AsyncRoutes: Array<RouteRecordRaw> = [
    ...DashboardAsyncRoutes,
    ...ErrorAsyncRoutes,
    ...AssemblyAsyncRoutes,
    ...OrgAsyncRoutes
]

export default AsyncRoutes;