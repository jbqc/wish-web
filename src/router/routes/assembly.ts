import { RouteRecordRaw } from "vue-router";
import { BasicLayout } from "/@/layouts/index"
import { t } from '/@/i18n/index'
/**
 * 演示模块路由
 */
const AssemblyAsyncRoutes: Array<RouteRecordRaw> = [
    {
        path: '/assembly',
        name: 'Assembly',
        meta: {
            menuId: 'assembly',
            title: t('menu.assembly.title'),
            icon: 'icon-block'
        },
        component: BasicLayout,
        redirect: '/assembly/editor',
        children: [
            {
                path: 'editor',
                name: 'AssemblyEditor',
                meta: {
                    menuId: 'assembly:00',
                    title: t('menu.assembly.richTextEditor.title'),
                    icon: 'icon-edit-square'
                },
                component: () => import('/@/views/assembly/editor/index.vue')
            },
            {
                path: 'page-table',
                name: 'AssemblyPageTable',
                meta: {
                    menuId: 'assembly:01',
                    title: t('menu.assembly.paginationTable.title'),
                    icon: 'icon-table'
                },
                component: () => import('/@/views/assembly/page-table/index.vue')
            },
            {
                path: 'map',
                name: 'Map',
                meta: {
                    menuId: 'assembly:02',
                    title: t('menu.assembly.map.title'),
                    icon: 'icon-table'
                },
                component: () => import('/@/views/assembly/map/qqmap/index.vue')
            }
        ]
    }
]

export default AssemblyAsyncRoutes;