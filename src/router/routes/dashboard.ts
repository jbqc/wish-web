import { RouteRecordRaw } from 'vue-router';
import { BasicLayout } from "/@/layouts/index"
import { t } from '/@/i18n/index'

/**
 * 看板模块路由
 */
const DashboardAsyncRoutes: Array<RouteRecordRaw> = [
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: BasicLayout,
        redirect: '/dashboard/analysis',
        meta: {
            menuId: 'dashboard',
            title: t('menu.dashboard.title'),
            subTitle: t('menu.dashboard.subTitle'),
            icon: 'icon-dashboard'
        },
        children: [
            {
                path: 'analysis',
                name: 'DashboardAnalysis',
                meta: {
                    menuId: 'dashboard:00',
                    title: t('menu.dashboard.analysis.title'),
                    subTitle: t('menu.dashboard.analysis.subTitle'),
                    icon: 'icon-heart'
                },
                component: () => import('/@/views/dashboard/analysis/index.vue')
            },
            {
                path: 'monitor',
                name: 'DashboardMonitor',
                meta: {
                    menuId: 'dashboard:01',
                    title: t('menu.dashboard.monitor.title'),
                    subTitle: t('menu.dashboard.monitor.subTitle'),
                    icon: 'icon-heart'
                },
                component: () => import('/@/views/dashboard/monitor/index.vue')
            },
            {
                path: 'user-info',
                name: 'DashboardUserInfo',
                meta: {
                    menuId: 'dashboard:02',
                    title: t('menu.dashboard.userInfo.title'),
                    subTitle: t('menu.dashboard.userInfo.subTitle'),
                    icon: 'icon-heart'
                },
                component: () => import('/@/views/dashboard/user-info/index.vue')
            }
        ]
    },
]
export default DashboardAsyncRoutes;