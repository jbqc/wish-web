import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
/**
 * 获取今夕何夕博客信息
 * @returns 
 */
export function getBlogList(): Promise<AxiosResponse<any>> {
    return service.get("/external/jxhxBlog/getBlogList");
}