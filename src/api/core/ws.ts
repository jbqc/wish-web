/** web socket base url 需要自行添加sockerId参数 */
export const webScoketBaseUrl = "ws://" + location.hostname + "/wish/webSocket.ws/";

/**
 * 获取 webSocketUrl
 * @param socketId  socketId
 * @returns 
 */
export const getWebSocketUrl = (socketId: string) => webScoketBaseUrl + socketId;