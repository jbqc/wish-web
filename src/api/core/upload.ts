import service from "/@/kit/request-kit";

export const uploadUrl = "/api/org/fileService/upload";
export const downloadUrl = "/api/org/fileService/downloadFile";
export const cdnUrl = "https://cdn.jiubanqingchen.cn"

/**
 * 下载附件url
 * @param fileId 文件id
 */
export function downloadFile(fileId: string) {
    return service.get(downloadUrl, { params: { fileId } });
}