import { AxiosResponse } from "axios";
import { LoginUser } from "/@/_types/user";
import service from "/@/kit/request-kit";


/**
 * 登录接口
 * @param loginUser
 */
export function login(loginUser: LoginUser): Promise<AxiosResponse<any>> {
    return service.get("/login", { params: loginUser });
}

/**
 * 注销登录
 */
export function logout(): Promise<AxiosResponse<any>> {
    return service.get("/logout");
}

export function getUserInfo(): Promise<AxiosResponse<any>> {
    return service.get("/org/user/info");
}
