/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
import { ModelApi } from "/@/_types/api";

class DataSourceApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const dataSourceApi = new DataSourceApi("/org/dataSource", "dataSourceId");

export default dataSourceApi;