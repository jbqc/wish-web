/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class NoticeApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    getNewsNotice(): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + "getNewsNotice");
    }
}
const noticeApi = new NoticeApi("/org/notice", "noticeId");

export default noticeApi;