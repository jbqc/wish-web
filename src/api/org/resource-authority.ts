import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class ResourceAuthorityApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    /**
     * 获取角色的菜单权限
     * @param roleId 角色id
     * @returns 菜单id数组
     */
    getRoleMenuResource(roleId: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "getRoleMenuResource", { params: { roleId } });
    }

    /**
   * 获取角色的接口权限
   * @param roleId 角色id
   * @returns 接口id数组
   */
    getRoleInterfaceResource(roleId: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "getRoleInterfaceResource", { params: { roleId } });
    }


}
const resourceAuthorityApi = new ResourceAuthorityApi("/org/resourceAuthority", "resourceAuthorityId");

export default resourceAuthorityApi;