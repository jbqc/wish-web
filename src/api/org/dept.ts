import { ModelApi } from "/@/_types/api";

class DeptApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const deptApi = new DeptApi("/org/dept", "deptId");

export default deptApi;