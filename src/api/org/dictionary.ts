import { ModelApi } from "/@/_types/api";

class DictionaryApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const dictionaryApi = new DictionaryApi("/org/dictionary", "dictionaryId");

export default dictionaryApi;