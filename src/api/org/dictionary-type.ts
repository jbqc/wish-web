import { ModelApi } from "/@/_types/api";

class DictionaryTypeApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const dictionaryTypeApi = new DictionaryTypeApi("/org/dictionaryType", "dictionaryTypeId");

export default dictionaryTypeApi;