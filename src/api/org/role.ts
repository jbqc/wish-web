import { ModelApi } from "/@/_types/api";

class RoleApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const roleApi = new RoleApi("/org/role", "roleId");

export default roleApi;