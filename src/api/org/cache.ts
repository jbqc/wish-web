/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class CacheApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法

    /**
    * 删除当前前缀的所有缓存key
    * @param cacheName 缓存名称
    * @param prefix  前缀
    * @returns 
    */
    deleteKeysByPrefixKey(cacheName: string, prefix: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "deleteKeysByPrefixKey", { params: { cacheName, prefix } });
    }
    /**
     * 删除所有的缓存
     * @param cacheName 缓存名称
     * @returns 
     */
    deleteAllCache(cacheName: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "deleteAllCache", { params: { cacheName } });
    }
}
const cacheApi = new CacheApi("/org/cache", "cacheId");

export default cacheApi;