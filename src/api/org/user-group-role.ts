import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class UserGroupRoleApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    userRoleList(queryParam: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "userRoleList", { params: queryParam });
    }
}
const userGroupRoleApi = new UserGroupRoleApi("/org/userGroupRole", "userGroupRoleId");

export default userGroupRoleApi;