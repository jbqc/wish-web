import { ModelApi } from "/@/_types/api";

class UserDeptApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const userDeptApi = new UserDeptApi("/org/userDept", "userDeptId");

export default userDeptApi;