import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class AppActionApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    /**
     * 初始化model controller 的action
     * @param {String} actionId 类的主键id
     * @returns 
     */
    initModelControllerAction(actionId: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "initModelControllerAction", { params: { actionId } });
    }
    /**
     * 获取接口树
     * @param queryParam 查询参数
     * @returns 
     */
    tree(queryParam?: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "tree", { params: queryParam });
    }
}
const appActionApi = new AppActionApi("/org/appAction", "actionId");

export default appActionApi;