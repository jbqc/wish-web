/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
import { ModelApi } from "/@/_types/api";

class LoginLogApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const loginLogApi = new LoginLogApi("/org/loginLog", "loginLogId");

export default loginLogApi;