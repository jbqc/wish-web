import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class CronTaskApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    /**
     * 初始化model controller 的action
     * @param {string} cronTaskId 任务id
     * @param {boolean} active 任务状态
     * @returns 
     */
    changeActive(cronTaskId: string, active: boolean): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "changeActive", { params: { cronTaskId, active } });
    }
    /**
     * 执行一次任务
     * @param cronTaskId 
     * @returns 
     */
    runOnceJob(cronTaskId: string): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + "runOnceJob", { params: { cronTaskId } });
    }
}
const cronTaskApi = new CronTaskApi("/org/cronTask", "cronTaskId");

export default cronTaskApi;