import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";
import { ModelApi } from "/@/_types/api";

class UserApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
    uploadExcel(formData: FormData): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + "uploadExcel", formData);
    }
    uploadAvatar(formData: FormData): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + "uploadAvatar", formData);
    }
}
const userApi = new UserApi("/org/user", "userId");

export default userApi;