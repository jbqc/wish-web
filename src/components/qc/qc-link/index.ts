import { App } from "vue"
import Link from './index.vue'


const QcLink = {
    install: function (app: App) {
        app.component('qc-link', Link)
    }
}

export default QcLink;