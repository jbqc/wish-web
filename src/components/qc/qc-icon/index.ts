
import { App } from "vue";
import { createFromIconfontCN } from "@ant-design/icons-vue";

const AntDesignIcon = createFromIconfontCN({
  scriptUrl: "//at.alicdn.com/t/font_2372190_jubowpn005a.js",// 在 iconfont.cn 上生成
});

const QcIcon = {
  install: function (app: App) {
    app.component('qc-icon', AntDesignIcon)
  }
}

export default QcIcon;
