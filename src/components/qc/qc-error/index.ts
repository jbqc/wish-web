import { App } from 'vue';
import Error from './server-error.vue'

const QcError = {
    install: function (app: App) {
        app.component('qc-error', Error)
    }
}

export default QcError;