import { App } from 'vue'
import ExcelUpload from './qc-excel-upload/index.vue'

const QcExcelUpload = {
    install: function (app: App) {
        app.component('qc-excel-upload', ExcelUpload)
    }
}

export {
    QcExcelUpload
}