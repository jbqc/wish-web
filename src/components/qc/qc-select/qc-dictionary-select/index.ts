import { App } from 'vue';
import DictionarySelect from './index.vue'

const QcDictionarySelect = {
    install: function (app: App) {
        app.component('qc-dictionary-select', DictionarySelect);
    }
}

export default QcDictionarySelect;