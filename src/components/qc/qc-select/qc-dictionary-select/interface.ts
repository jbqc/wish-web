export const dictionaryPorps = {
    //数据字典标识
    codeItemId: {
        required: true,
        type: String
    },
    //值
    value: {
        type: String
    },
    size: {
        type: String,
        default: "small"
    },
    placeholder: {
        type: String,
    },
    allowClear: {
        type: Boolean,
        default: true
    },
    //是否只读
    disabled: {
        type: Boolean,
        default: false
    }
}