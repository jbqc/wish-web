import { App } from 'vue';
import JsonPretty from './json-pretty.vue'

const QcJsonPretty = {
    install: function (app: App) {
        app.component('qc-json-pretty', JsonPretty)
    }
}

export default QcJsonPretty;