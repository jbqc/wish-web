import { ColumnProps } from "ant-design-vue/es/table/interface";
import { defaultTableProps } from "ant-design-vue/es/table/Table";

export type PageTableColumnProps = ColumnProps & {
    hidden?: Boolean,
    excelKey?: String//导出excel的key
}

export const pageTableProps = Object.assign({}, defaultTableProps, {
    //大小 默认使用最小的
    size: {
        type: String,
        default: 'small'
    },
    //是否有边框 默认有边框会好看一点
    bordered: {
        type: Boolean,
        default: false
    },
    //分页信息设置
    pagination: {
        type: Object,
    },
    //自定义的表格类型 multiple single null
    tableSelection: {
        type: String,
        default: 'multiple'
    },
    toolbarTitle: {
        type: String
    },
    loadData: {
        type: Function
    },
    //下载excel
    downloadExcel: {
        type: Function
    },
    //设置toolbar显示那些插件按钮
    showToolbar: {
        type: Array,
        default: () => ["reload", "downloadExcel", "density", "columnDisplay"],
    },
    //整体是否有边框
    cardBordered: {
        type: Boolean,
        default: false
    },
    helper: {
        type: Object,
        default: () => {
            return {
                serachTable: null,
                reloadTable: null,
                getSelectData: null,
            }
        }
    },
    //导出excel的名称
    downloadExcelName: {
        type: String,
    }
})

export type PageTableHelper = {
    serachTable: Function,
    reloadTable: Function,
    getSelectData: Function
}
/**
 * 表格选中类型
 */
export type TableSelectedData = {
    selectedRowKeys: Array<string>
    selectedRows: Array<any>
}