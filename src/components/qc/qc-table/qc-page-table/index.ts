import { App } from 'vue';
import PageTable from './page-table'

const QcPageTable = {
    install: function (app: App) {
        app.component('qc-page-table', PageTable);
    }
}

export default QcPageTable;