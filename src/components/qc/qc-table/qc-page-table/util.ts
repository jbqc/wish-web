import { Slot } from "vue";

export type InternalSlots = {
    [name: string]: Slot | undefined;
};

export type PageTableSlots = {
    aTableSlots: InternalSlots;
    toolbarSlots: InternalSlots;
}

/**
* 处理page-table的slots
* @param slots 
*/
export function pageTableSlotsHandler(slots: InternalSlots): PageTableSlots {
    const _slots = { ...slots };
    const pageTableSlots: PageTableSlots = { aTableSlots: {}, toolbarSlots: {} };
    if (_slots === undefined) return pageTableSlots;
    //toolbar ==> toolbarTitle toolbarButton
    if (_slots.toolbarTitle) {
        pageTableSlots.toolbarSlots.toolbarTitle = _slots.toolbarTitle
        delete _slots.toolbarTitle;
    }
    if (_slots.toolbarButton) {
        pageTableSlots.toolbarSlots.toolbarButton = _slots.toolbarButton
        delete _slots.toolbarButton;
    }
    //不是toolbar的slot 默认为a-table
    pageTableSlots.aTableSlots = _slots;
    return pageTableSlots;
}

/**
 * 处理查询参数
 * @param pagination 分页参数
 * @param filters 过滤参数
 * @param sorter 排序
 * @param config 配置
 */
export function queryParamHandler(pagination: any, filters: any, sorter: any, config: any): any {
    //处理分页
    const queryparam: any = { page: pagination.current, limit: pagination.pageSize }
    //处理filter
    if (filters) {
        for (let key in filters) {
            if (filters[key] && filters[key].length > 0) {
                queryparam[key] = filters[key].join(",")
            }
        }
    }
    //处理sorter //可能出现不排序情况 这个时候的sorter就不用做排序了 此时order为空
    if (sorter && sorter.order) {
        queryparam.orders = { field: sorter.field, order: sorter.order }
    }
    return queryparam;
}