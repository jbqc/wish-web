import { computed, defineComponent, getCurrentInstance, h, reactive, Ref, ref } from "vue"
import { useI18n } from 'vue-i18n'
import { PageTableColumnProps, PageTableHelper, pageTableProps, TableSelectedData } from "./interface";
import TableToolbar from './component/toolbar.vue'
import { AxiosResponse } from "axios";
import { InternalSlots, pageTableSlotsHandler, queryParamHandler } from "./util";
import { ColumnProps } from "ant-design-vue/es/table/interface";
import { message, Modal, Spin } from "ant-design-vue";
import { downloadFile, replaceAllForStringByRegExp } from "/@/kit/common-kit";
import { removeStr } from "/@/kit/array-kit";
import ServerError from "/@/components/qc/qc-error/server-error.vue";

const PageTable = defineComponent({
    name: 'PageTable',
    props: pageTableProps,
    emits: ['pageTableChange', 'pageTableReload', "rowSelectChange"],
    components: {
        TableToolbar
    },
    setup(props, { emit }) {
        const { t } = useI18n();
        const tableStateProps = reactive({ ...props });
        const ctx = getCurrentInstance();
        const slots = ctx?.slots;
        const pageTableSlots = pageTableSlotsHandler(slots as InternalSlots);

        //表格内部参数缓存
        let cacheQueryParams = {};

        //表格全局laoding
        const pageTableSpinState = reactive({
            tip: null as string,
            spinning: false,
        })

        //定义filter 与sorter 在表格刷新时清理掉他们
        const filteredInfo = ref();
        const sortedInfo = ref();

        //自定义选中数据 选中key数组
        const tableSelectedData: TableSelectedData = reactive({
            selectedRowKeys: [],
            selectedRows: [],
        });

        //toolbar的一些参数
        const toolbarState = reactive({
            checkedKeys: [] as Array<string>
        })

        //tabele选中触发事件
        const tableRowSelection = computed(() => {
            if (tableStateProps.tableSelection == "multiple") {
                return {
                    selectedRowKeys: tableSelectedData.selectedRowKeys,
                    onChange: handlerRowSelectChange,
                };
            } else if (tableStateProps.tableSelection == "single") {
                return {
                    type: 'radio',
                    selectedRowKeys: tableSelectedData.selectedRowKeys,
                    onChange: handlerRowSelectChange,
                };
            } else {
                return null;
            }
        });
        tableStateProps.rowSelection = tableRowSelection;

        //处理行号信息
        const noColumns: Array<ColumnProps> = [{
            title: t('component.table.no'),
            dataIndex: 'page_table_no',
            align: 'center',
            width: 50,
            customRender({ index }) {
                return (tableStateProps.pagination.current - 1) * tableStateProps.pagination.pageSize + index + 1
            }
        }]

        const handlerColumns = noColumns.concat(tableStateProps.columns as Array<any>);
        //处理filters sorter 信息
        tableStateProps.columns = computed(() => {
            const filtered = filteredInfo.value || {};
            const sorted = sortedInfo.value || {};
            handlerColumns.forEach(item => {
                if (item.sorter === true) {
                    item.sortOrder = sorted.columnKey === item.dataIndex && sorted.order
                }
                if (item.filters) {
                    item.filteredValue = filtered[item.dataIndex] || null
                }
            })
            return handlerColumns;
        })

        //处理分页信息
        if (!tableStateProps.pagination) {
            tableStateProps.pagination = {
                pageSize: 10, // 默认每页显示数量
                showSizeChanger: true, // 显示可改变每页数量
                pageSizeOptions: ["10", "20", "25", "50", "100"], // 每页数量选项
                showTotal: (total: number) => {
                    return t('component.table.totalItems', { total: total });
                }, // 显示总数
                total: 0, //总条数
                current: 1
            };
        }

        //显示列   
        const showColumns = computed(() => {
            //这里后续可能按照选中的key值做排序 可以后续添加这个功能
            return _columns.filter((item) => toolbarState.checkedKeys.indexOf(item.dataIndex) != -1)
        })

        //////////////////////////////////////////////////对外触发事件///////////////////////////////////////////////////////////////////
        /**
         *  加载表格
         */
        const loadTable = () => {
            tableStateProps.loading = true;
            const queryparam = queryParamHandler(tableStateProps.pagination, filteredInfo.value, sortedInfo.value, null)
            if (queryparam) cacheQueryParams = { ...queryparam };
            tableStateProps.loadData(queryparam).then((res: AxiosResponse<any>) => {
                tableStateProps.dataSource = res.data.data.list;
                tableStateProps.pagination.total = res.data.data.totalRow;
            }).finally(() => tableStateProps.loading = false)
        }

        //表格change事件
        const onATableChange = (pagination: any, filters: any, sorter: any, xx: any) => {
            //改变表格的分页信息
            tableStateProps.pagination.current = pagination.current;
            tableStateProps.pagination.pageSize = pagination.pageSize;
            //配置filter 与sorter对象接收
            filteredInfo.value = filters;
            sortedInfo.value = sorter;
            loadTable();//触发查询
            emit("pageTableChange", pagination, filters, sorter, xx)
        }
        /**
        * 选中变化时
        */
        const handlerRowSelectChange = (
            selectedRowKeys: Array<string>,
            selectedRows: Array<any>
        ) => {
            tableSelectedData.selectedRowKeys = selectedRowKeys;
            tableSelectedData.selectedRows = selectedRows;
            emit("rowSelectChange", selectedRowKeys, selectedRows, tableSelectedData);
        };

        /////////////////////////////////////toolbar//////////////////////////
        //处理列表已经得到默认显示的字段列 配合toolbar组件
        const _columns = tableStateProps.columns as Array<PageTableColumnProps>;

        //处理按钮的信息
        if (!tableStateProps.downloadExcel) {
            if (tableStateProps.showToolbar) tableStateProps.showToolbar = removeStr(tableStateProps.showToolbar as Array<string>, 'downloadExcel');
        }

        _columns.forEach((item) => {
            if (item.hidden !== true) toolbarState.checkedKeys.push(item.dataIndex);
        });

        //重载事件
        const onReload = () => {
            handlerRowSelectChange([], []);
            //清理filter 与sorter信息
            filteredInfo.value = null;
            sortedInfo.value = null;
            loadTable();//获取分页的信息
            emit("pageTableReload");
        }

        //下载excel
        const onDownloadExcel = () => {
            //移除前端序号 //以及操作列
            const removeArr: Array<string> = [noColumns[0].dataIndex];
            const excelDataArr = [];
            showColumns.value.forEach(item => {
                if (removeArr.indexOf(item.dataIndex) == -1) {
                    //有限选择配置的excelKey
                    excelDataArr.push({ key: item.excelKey || item.dataIndex, title: item.title })
                }
            })
            pageTableSpinState.tip = t('component.table.toolbar.downloadingExcel');
            pageTableSpinState.spinning = true;
            console.log(cacheQueryParams)
            tableStateProps.downloadExcel({ ...cacheQueryParams, excelName: tableStateProps.downloadExcelName, excelDataArr }).then((response: AxiosResponse<any>) => {
                const fileBlob = response.data as Blob;
                if (fileBlob.type == "application/json") {
                    let reader = new FileReader();
                    reader.readAsText(fileBlob);
                    reader.onload = (e) => {
                        //message.error(JSON.parse(e.target.result as string).msg)
                        Modal.error({
                            title: t('kit.request.errorTitle'),
                            width: 1200,
                            //content: h('div', {}, arr),
                            content: h(ServerError, { errorData: JSON.parse(e.target.result as string) })
                        });
                    }
                } else {
                    //临时处理一下 英文空格好像变成了+号
                    let fileName = replaceAllForStringByRegExp(response.headers.filename, RegExp("\\+", "g"), "-");
                    downloadFile({
                        fileName: decodeURI(fileName),
                        blob: fileBlob,
                    });
                }
            }).finally(() => pageTableSpinState.spinning = false)
        }

        // const getSelectData = () => {
        //     return tableSelectedData;
        // }

        //设置hepler 为使用者提供服务
        tableStateProps.helper as PageTableHelper;
        tableStateProps.helper.reloadTable = onReload;
        tableStateProps.helper.serachTable = loadTable;
        tableStateProps.helper.getSelectData = () => tableSelectedData;

        return () => (
            <>
                <div class="qc-page-table">
                    <Spin tip={pageTableSpinState.tip} spinning={pageTableSpinState.spinning}>
                        <a-card bordered={tableStateProps.cardBordered}>
                            <div class="qc-page-table-container">
                                <div class="qc-page-table-header">
                                    <TableToolbar
                                        columns={_columns}
                                        checkedKeys={toolbarState.checkedKeys}
                                        toolbarTitle={props.toolbarTitle}
                                        showToolbar={tableStateProps.showToolbar}
                                        v-models={[[tableStateProps.size, "tableSize"], [toolbarState.checkedKeys, "checkedKeys"]]}
                                        v-slots={pageTableSlots.toolbarSlots}
                                        onReload={onReload}
                                        onDownloadExcel={onDownloadExcel}
                                    >
                                    </TableToolbar>
                                </div>
                                <div class="qc-page-table-content">
                                    <a-table
                                        {...tableStateProps}
                                        dataSource={tableStateProps.dataSource}
                                        columns={showColumns.value}
                                        v-slots={slots}
                                        onChange={onATableChange}>
                                    </a-table>
                                </div>
                            </div>
                        </a-card>
                    </Spin>
                </div>
            </>
        )

    }
})

export default PageTable;
