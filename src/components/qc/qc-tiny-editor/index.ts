import { App } from 'vue';
import TinyEditor from './tiny-editor.vue'

const QcTinyEditor = {
    install: function (app: App) {
        app.component('qc-tiny-editor', TinyEditor)
    }
}

export default QcTinyEditor;