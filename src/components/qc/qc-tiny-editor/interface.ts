export const defaultPlugins = 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image ' +
    'link media template code codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists wordcount textpattern autosave ';

export const defaultToolbar = 'fullscreen undo redo restoredraft | cut copy paste pastetext | forecolor backcolor bold italic underline strikethrough link anchor | ' +
    'alignleft aligncenter alignright alignjustify outdent indent | styleselect formatselect fontselect fontsizeselect | bullist numlist | ' +
    ' blockquote subscript superscript removeformat | table image media charmap hr pagebreak insertdatetime print preview | ' +
    'code selectall searchreplace visualblocks | indent2em lineheight formatpainter axupimgs';


export const EditorProps = {
    //插件
    plugins: {
        type: String,
        default: defaultPlugins
    },
    //工具栏
    toolBar: {
        type: String,
        default: defaultToolbar
    },
    //菜单工具栏
    menuBar: {
        type: [String, Boolean],
        default: true
    },
    //是否禁用编辑
    disabled: {
        type: Boolean,
        default: false
    },
    //高度
    height: {
        type: Number,
        default: 400
    },
    //文章类容
    content: {
        type: String,
        default: ''
    },
}