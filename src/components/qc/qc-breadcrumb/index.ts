import { App } from 'vue';
import Breadcrumb from './breadcrumb.vue'

const QcBreadcrumb = {
    install: function (app: App) {
        app.component('qc-breadcrumb', Breadcrumb)
    }
}

export default QcBreadcrumb;