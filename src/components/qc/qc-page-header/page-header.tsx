import { defineComponent, getCurrentInstance, reactive } from "vue";
import { PageHeaderProps } from './interface'
import { PageHeader } from 'ant-design-vue'
import { useRoute } from "vue-router";
import { isBlankForString, isBlank } from "/@/kit/blank-kit";
/**
 * QcPageHeader组件
 */
const _QcPageHeader = defineComponent({
    name: 'PageTable',
    props: PageHeaderProps,
    emits: ['back'],
    setup(props, { emit }) {
        const route = useRoute();
        const pageHeaderStateProps = reactive({ ...props });
        //获取vue实例 并拿到slots
        const ctx = getCurrentInstance();
        const slots = ctx?.slots;

        //如果父组件没有title subtitle的插槽使用 且没有传入title subtitle的prop则取路由配置
        if (isBlankForString(pageHeaderStateProps.title) && isBlank(slots.title)) {
            pageHeaderStateProps.title = route.meta.title as string;
        }
        if (isBlankForString(pageHeaderStateProps.subTitle) && isBlank(slots.subTitle)) {
            pageHeaderStateProps.subTitle = route.meta.subTitle as string;
        }

        //如果外面不传back返回方法 则不触发
        let onBackFunc = null;
        if (pageHeaderStateProps.onBack) {
            onBackFunc = () => emit('back')
        }
        return () => (
            <>
                <div>
                    <PageHeader
                        class="common-page-header"
                        title={pageHeaderStateProps.title}
                        subTitle={pageHeaderStateProps.subTitle}
                        v-slots={slots}
                        onBack={onBackFunc}
                    >
                    </PageHeader>
                </div>
            </>
        )
    }
})

export default _QcPageHeader;