import { App } from 'vue'
import PageHeader from './page-header'

const QcPageHeader = {
    install: (app: App) => {
        app.component('qc-page-header', PageHeader)
    }
}

export default QcPageHeader