import { pageHeaderProps } from 'ant-design-vue/es/page-header'

export const PageHeaderProps = Object.assign({}, {}, {
    //这里存放多余的类型
    title: {
        type: String
    },
    subTitle: {
        type: String
    },
    onBack: {
        type: Function
    }
})