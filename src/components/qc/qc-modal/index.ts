import { App } from "vue"
import BasicModal from './qc-basic-modal/index.vue'


const QcBasicModal = {
    install: function (app: App) {
        app.component('qc-basic-modal', BasicModal)
    }
}

export { QcBasicModal };