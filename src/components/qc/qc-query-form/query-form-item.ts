import { defineComponent, h, VNode } from "vue";
import { QueryFormItemProps, QueryFormItem, QueryFormSelectItemOption } from './interface'
import { Input, DatePicker, Select } from 'ant-design-vue'
import QcDictionarySelect from '/@/components/qc/qc-select/qc-dictionary-select/index.vue'
import { useI18n } from 'vue-i18n'

const QcQueryFormItem = defineComponent({
    name: 'QcQueryFormItem',
    props: QueryFormItemProps,
    emits: ["searchForm"],
    setup(props, { emit }) {
        const { t } = useI18n();
        const { queryParam, queryFormItem } = props;

        /**
         * 在input输入框中按enter键可以直接进行搜索
         * @param event 
         */
        const onKeyupEnter = (event: any) => {
            if (event.keyCode !== 13) return
            handlerSerach();
        }

        /**
         * 触发enter事件
         */
        const handlerSerach = () => emit("searchForm");

        /**
         * 渲染dom
         * @param queryFormItem 
         */
        const renderAntFormItem = (queryFormItem: QueryFormItem): VNode => {
            let vnode = null;
            switch (queryFormItem.type) {
                case "input":
                    vnode = renderInputToFormItem(queryFormItem);
                    break;
                case "date":
                    vnode = renderDateToFormItem(queryFormItem);
                    break;
                case "select":
                    vnode = renderSelectToFormItem(queryFormItem);
                    break;
                case "dictionarySelect":
                    vnode = renderDictionarySelectToFormItem(queryFormItem);
                    break;
            }
            return vnode;
        }

        /**
         * 渲染下拉选择
         */
        const renderSelectToFormItem = (queryFormItem: QueryFormItem): VNode => {
            const arr: Array<VNode> = [];
            queryFormItem.selectData.forEach((option: QueryFormSelectItemOption) => {
                arr.push(h(Select.Option, { value: option.value }, {
                    default: () => {
                        return h('span', {}, option.label);
                    }
                }));
            })
            let placeholder = t('component.query-form.pleaseSelect') + " " + queryFormItem.label;
            return h(Select, {
                placeholder,
                allowClear: true,
                title: placeholder,
                value: queryParam[queryFormItem.name],
                'onUpdate:value': (value: any) => {
                    queryParam[queryFormItem.name] = value;
                    handlerSerach()
                },
            }, { default: () => arr });
        }

        /**
         * 渲染数据字典选择
         * @param queryFormItem 
         * @returns 
         */
        const renderDictionarySelectToFormItem = (queryFormItem: QueryFormItem): VNode => {
            let codeItemId = queryFormItem.codeItemId;
            let placeholder = t('component.query-form.pleaseSelect') + " " + queryFormItem.label;
            return h(QcDictionarySelect, {
                placeholder,
                codeItemId,
                title: placeholder,
                value: queryParam[queryFormItem.name],
                'onUpdate:value': (value: any) => {
                    queryParam[queryFormItem.name] = value;
                    handlerSerach()
                },
            })
        }

        /**
         * 渲染input
         */
        const renderInputToFormItem = (queryFormItem: QueryFormItem): VNode => {
            let placeholder = t('component.query-form.pleaseInput') + " " + queryFormItem.label
            return h(Input, {
                placeholder,
                title: placeholder,
                value: queryParam[queryFormItem.name],
                'onUpdate:value': value => queryParam[queryFormItem.name] = value,
                onPressEnter: (event: any) => handlerSerach()
            });
        }

        /**
         * 渲染日期框
         */
        const renderDateToFormItem = (queryFormItem: QueryFormItem): VNode => {
            let valueFormat = queryFormItem.valueFormat || 'YYYY-MM-DD';
            let placeholder = t('component.query-form.pleaseSelect') + " " + queryFormItem.label
            return h(DatePicker, {
                placeholder,
                valueFormat,
                title: placeholder,
                value: queryParam[queryFormItem.name],
                'onUpdate:value': (value: any) => {
                    queryParam[queryFormItem.name] = value;
                    handlerSerach()
                },
            });
        }

        return () => {
            if (queryFormItem.customRender) {
                const nodeTemplate: Function = queryFormItem.customRender;
                return nodeTemplate(queryFormItem);
            } else {
                return renderAntFormItem(queryFormItem as QueryFormItem);
            }
        }
    }
})

export default QcQueryFormItem;