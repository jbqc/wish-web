export function getSlotsCustomRenderMap(columns: Array<any>): Map<number, string> {
    if (!columns || columns.length == 0) return null;
    const resultMap: Map<number, string> = new Map();
    columns.forEach((item, index) => {
        if (item.slots && item.slots.customRender) {
            resultMap.set(index, item.slots.customRender);
        }
    })
    return resultMap;
}