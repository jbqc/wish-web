export type QueryFormSelectItemOption = {
    label: string;
    value: string;
}

/**
 * 查询表格列 暂时不想拆分了
 */
export type QueryFormItem = {
    name: string;//表单名称
    label: string;//ant-form-item-label
    type: string;//类型
    customRender?: Function;
    slots?: any;//插槽对象
    placeholder?: string;//显示项
    valueFormat?: string;//这个是date独有的
    selectData?: Array<QueryFormSelectItemOption>;//这个是select 独有
    codeItemId?: string;//数据字典查询独有
}

export const QueryFormItemProps = {
    //查询对象
    queryParam: Object,
    type: {//类型 input date select dictionary-select 等等
        type: String,
        default: 'input'
    },
    //queryFormItem: QueryFormItem,
    queryFormItem: Object
}

export const QueryFormProps = {
    colSpan: {
        //24/colSpan 得出一行有几列搜索条件
        type: Number,
        default: 8,
    },
    //搜索的data 可支持 input select date //todo根据需要进行持续增加 [{name,type,label}]
    queryColumn: {
        type: Array,
        default: () => { return [] }
    },
    //需要放到后台中查询的值,需要searchData中的name相同
    queryParam: {
        type: Object,
        default: () => { return {} },
    },
    labelCol: {
        type: Object,
        default: () => {
            return { span: 8 };
        },
    },
    wrapperCol: {
        type: Object,
        default: () => {
            return { span: 16 };
        },
    }
}