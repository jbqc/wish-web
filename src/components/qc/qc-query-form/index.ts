import { App } from 'vue';
import QueryForm from './index.vue'

const QcQueryForm = {
    install: function (app: App) {
        app.component('qc-query-form', QueryForm)
    }
}

export default QcQueryForm;
