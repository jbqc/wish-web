import { App } from 'vue';
import AddButton from './add-button.vue'
import DeleteButton from './delete-button.vue'
import SaveButton from './save-button.vue'
import CancelButton from './cancel-button.vue'
import UploadButton from './upload-button.vue'

const QcAddButton = {
    install: function (app: App) {
        app.component('qc-add-button', AddButton)
    }
}

const QcDeleteButton = {
    install: function (app: App) {
        app.component('qc-delete-button', DeleteButton)
    }
}

const QcSaveButton = {
    install: function (app: App) {
        app.component('qc-save-button', SaveButton)
    }
}

const QcCancelButton = {
    install: function (app: App) {
        app.component('qc-cancel-button', CancelButton)
    }
}

const QcUploadButton = {
    install: function (app: App) {
        app.component('qc-upload-button', UploadButton)
    }
}

export { QcAddButton, QcCancelButton, QcDeleteButton, QcSaveButton, QcUploadButton };