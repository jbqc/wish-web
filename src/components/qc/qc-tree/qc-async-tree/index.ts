import { App } from 'vue';
import AsyncTree from './index.vue'

const QcAsyncTree = {
    install: function (app: App) {
        app.component('qc-async-tree', AsyncTree);
    }
}

export default QcAsyncTree;