/**
 * 异步树的配置信息
 */
export const AsyncTreeProps = {
    rootId: {//根节点id默认为0
        default: "0"
    },
    replaceFields: {
        type: Object,
        default: () => {
            return {
                key: 'id',
                parentKey: 'parentId',
                title: 'title'
            }
        }
    },
    autoExpandParent: {//是否自动展开父节点
        type: Boolean,
        default: true
    },
    showActionIcon: {//是否显示操作按钮图标
        type: Boolean,
        default: false
    },
    //数据获取方法
    loadData: {
        required: true,
        type: Function
    },
    //节点前添加 Checkbox 复选框
    checkable: {
        type: Boolean,
        default: false
    },
    //checkable 状态下节点选择完全受控（父子节点选中状态不再关联）
    checkStrictly: {
        type: Boolean,
        default: true
    },
    //checkable 状态下 默认选中的节点
    checkedKeys: {
        type: [Array, Object],
    },
    //checkable 状态下 禁止选中的节点
    disabledKeys: {
        type: Array,
    }
}