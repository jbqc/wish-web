/*
 * @Descripttion: 
 * @version: 0.0.1
 * @Author: zhu_liangyu
 * @Date: 2020-11-09 17:50:01
 * @LastEditors: zhu_liangyu
 * @LastEditTime: 2020-11-09 21:02:04
 */
import GlobalHeader from '/@/layouts/basic-layout/component/global-header/index.vue'
import GlobalContent from '/@/layouts/basic-layout/component/global-content/index.vue'
import GlobalFooter from '/@/layouts/basic-layout/component/global-footer/index.vue'
import GlobalSider from '/@/layouts/basic-layout/component/global-sider/index.vue'
export { GlobalHeader, GlobalContent, GlobalFooter, GlobalSider }