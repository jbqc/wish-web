import BasicLayout from "/@/layouts/basic-layout/index.vue";
import BlankLayout from "/@/layouts/blank-layout/index.vue";

export { BasicLayout, BlankLayout }