import { createApp } from 'vue'
import route from './router'
import { store, key } from './store'
import App from './App.vue'
import { init } from './framework/init/use'
import 'ant-design-vue/dist/antd.css';
import '/@/styles/index.less'
import i18n from '/@/i18n/index'

const app = createApp(App);
init(app)
app.use(route).use(store, key);
app.use(i18n);
app.mount('#app');
