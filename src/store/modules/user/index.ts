import { ActionContext } from "vuex";
import { login, logout, getUserInfo } from '/@/api/core';
import { getLocale, setLocale } from "/@/kit/i18n-kit";
import { clearAccessToken, setAccessToken } from "/@/kit/token-kit";
import { createWebRouter } from "/@/router";
import { LoginUser, WishUser } from "/@/_types/user";
import NoticeApi from '/@/api/org/notice'
import { notification } from 'ant-design-vue';
const state: WishUser = {
    userId: null,//用户id
    userName: "久伴轻尘",//用户名称
    locale: getLocale(),//国际化语言
    token: null,//token数据
    menus: [],//动态菜单key
    userAvatar: null//用户头像id
}

const actions = {
    //修改语言
    changeLocale(ctx: ActionContext<WishUser, {}>, locale: string) {
        ctx.commit("SET_USER_LOCALE", locale);
        setLocale(locale);
        location.reload();
    }, //登录
    login(ctx: ActionContext<WishUser, {}>, loginUser: LoginUser) {
        return new Promise((resolve, reject) => {
            login(loginUser).then(res => {
                let token = res.data.data;
                setAccessToken(token);
                ctx.commit('SET_USER_TOKEN', token)
                //做一个获取通知的接口
                setTimeout(() => {
                    NoticeApi.getNewsNotice().then(res => {
                        notification.info({
                            message: res.data.data.title,
                            description: res.data.data.content
                        });
                    })
                }, 2000)
                resolve(res);
            }).catch(error => {
                reject(error)
            })
        })
    },
    //登出
    logout(ctx: ActionContext<WishUser, {}>) {
        return new Promise((resolve, reject) => {
            logout().then(() => {
                clearAccessToken();
                ctx.commit('CLEAR_USER_TOKEN');
                ctx.commit('CLEAR_USER_INFO');
                createWebRouter();
                resolve(true);
            }).catch(error => {
                reject(error)
            })
        })
    },
    getInfo(ctx: ActionContext<WishUser, {}>) {
        //处理路由 //todo 这个以后再对接后台系统 现在先简单适配
        return new Promise((resolve, reject) => {
            getUserInfo().then(res => {
                const wishUser: WishUser = res.data.data;
                ctx.commit("SET_USER_ID", wishUser.userId);
                ctx.commit("SET_USER_NAME", wishUser.userName);
                ctx.commit("SET_USER_LOCALE", wishUser.locale);
                ctx.commit("SET_USER_MENUS", wishUser.menus);
                ctx.commit("SET_USER_AVATAR", wishUser.userAvatar)
                resolve(wishUser);
            }).catch(error => {
                reject(error)
            })
        })
    },
    clearToken(ctx: ActionContext<WishUser, {}>) {
        ctx.commit("CLEAR_USER_TOKEN")
    }
}

const mutations = {
    //设置用户id
    SET_USER_ID(state: WishUser, userId: string) {
        state.userId = userId;
    },
    //设置姓名
    SET_USER_NAME(state: WishUser, userName: string) {
        state.userName = userName;
    },
    //设置语言
    SET_USER_LOCALE(state: WishUser, locale: string) {
        state.locale = locale;
    },
    //设置token
    SET_USER_TOKEN(state: WishUser, token: string) {
        state.token = token;
    },
    //设置路由
    SET_USER_MENUS(state: WishUser, menus: Array<string>) {
        state.menus = menus;
    },
    //设置用户头像
    SET_USER_AVATAR(state: WishUser, userAvatar: string) {
        state.userAvatar = userAvatar;
    },
    CLEAR_USER_TOKEN(state: WishUser) {
        state.token = null;
    },
    CLEAR_USER_INFO(state: WishUser) {
        state.userId = null;
        state.userName = null;
        state.token = null;
        state.menus = [];
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}