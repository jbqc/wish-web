import { InjectionKey } from 'vue'
import { createStore, useStore as baseUseStore, Store } from 'vuex'
import user from './modules/user'
import { WishUser } from '/@/_types/user'


export interface RootState {
    user: WishUser,
}

export const key: InjectionKey<Store<RootState>> = Symbol()

export const store = createStore<RootState>({
    modules: {
        user,
    }
})

export function useStore() {
    return baseUseStore(key)
}