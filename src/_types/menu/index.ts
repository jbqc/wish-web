/**
 * 服务于 页面菜单创建 keys==route path
 */
export class WishAntMenuObj {
    /**
     * 隐藏菜单key信息
     */
    hiddenMenuKeys?: Array<string>;
    /**
     * 菜单的根菜单key信息
     */
    rootSubmenuKeys?: Array<string>;
    /**
     * 所有key
     */
    allKes?: Array<string>;
    /**
     * 所有菜单信息
     */
    menus?: Array<WishAntMenu>
}

/**
 * 页面菜单对象
 */
export class WishAntMenu {
    /**
     * 菜单key信息
     */
    key: string;
    /**
     * 菜单图标信息
     */
    icon?: string;
    /**
     * 菜单路由地址 (路由全路径)
     */
    path: string;
    /**
     * 菜单名称
     */
    title: string;
    /**
     * 子菜单信息
     */
    children?: Array<WishAntMenu>
}

export class WishAntMenuTree {
    /**
     * 菜单id
     */
    menuId: string;
    /**
     * 菜单key信息
     */
    key: string;
    /**
     * 菜单图标信息
     */
    icon?: string;
    /**
     * 菜单路由地址 (路由全路径)
     */
    path: string;
    /**
     * 菜单名称
     */
    title: string;
    /**
     * 是否显示
     */
    hidden: boolean;
    /**
     * 子菜单信息
     */
    children?: Array<WishAntMenuTree>
}