import { AxiosResponse } from "axios";
import service from "/@/kit/request-kit";

/**
 * 定义url的key值
 */
const URL_KEY = {
    page: 'page',
    list: 'list',
    id: 'id/',
    add: 'add',
    addBatch: 'addBatch',
    newModel: 'newModel',
    update: 'update',
    updateBatch: 'updateBatch',
    delete: 'delete',
    deleteBatch: "deleteBatch",
    downloadExcel: "downloadExcel"
}


/**
 * 定义baseModel  对应后台modelController
 */
export interface IModelApi {
    primaryKey: String;
    list(queryParam?: any): Promise<AxiosResponse<any>>,
    page(queryParam?: any): Promise<AxiosResponse<any>>,
    id(id: string, extData?: any): Promise<AxiosResponse<any>>,
    add(data: any, extData?: any): Promise<AxiosResponse<any>>,
    addBatch(dataList: Array<any>, extData?: any): Promise<AxiosResponse<any>>,
    newModel(queryParam?: any): Promise<AxiosResponse<any>>,
    update(data: any, extData?: any): Promise<AxiosResponse<any>>,
    updateBatch(dataList: Array<any>, extData?: any): Promise<AxiosResponse<any>>,
    delete(data: any, extData?: any): Promise<AxiosResponse<any>>,
    deleteBatch(dataList: Array<any>, extData?: any): Promise<AxiosResponse<any>>,
    downloadExcel(queryParam?: any): Promise<AxiosResponse<any>>,
}

/**
 * 定义 BaseModel
 */
export class ModelApi implements IModelApi {
    primaryKey: string;
    protected baseUrl: string;//例 /org/user => list url => /org/user/list
    /**
     * 需要传入baseUrl
     * @param baseUrl baseurl 例：/org/user
     */
    constructor(baseUrl: string, primaryKey: string) {
        if (!baseUrl) throw new Error("baseUrl not null.");
        if (baseUrl.charAt(baseUrl.length - 1) != "/") baseUrl += "/"
        this.baseUrl = baseUrl;
        this.primaryKey = primaryKey;
    }

    /**
     * 查询所有
     * @param queryParam 查询参数
     * @param customUrl 自定义查询url
     */
    list(queryParam?: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + URL_KEY.list, { params: queryParam });
    }
    /**
     * 分页查询
     * @param queryParam 分页查询参数
     * @param customUrl 自定义分页查询url
     */
    page(queryParam?: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + URL_KEY.page, { params: queryParam });
    }
    /**
     * 查询指定id数据
     * @param id  id
     * @param extData  额外参数
     * @param customUrl 自定义查询url
     */
    id(id: string, extData?: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + URL_KEY.id + id, { params: extData });
    }
    /**
     * 新增一条记录
     * @param data 记录
     * @param extData 额外参数
     * @param customUrl 自定义url
     */
    add(data: any, extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.add, data, { params: extData });
    }
    /**
     * 新增一个model 在表单新增时执行
     * @param param 参数
     */
    newModel(param?: any): Promise<AxiosResponse<any>> {
        return service.get(this.baseUrl + URL_KEY.newModel, { params: param });
    }
    /**
     * 新增多条记录
     * @param dataList 记录列表
     * @param extData 额外参数
     * @param customUrl 自定义url
     */
    addBatch(dataList: any[], extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.addBatch, dataList, { params: extData });
    }
    /**
     * 更新一条记录
     * @param data 记录
     * @param extData 额外参数
     * @param customUrl 自定义url
     */
    update(data: any, extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.update, data, { params: extData });
    }
    /**
     * 更新多条记录
     * @param dataList 记录列表
     * @param extData 额外参数
     * @param customUrl 自定义url
     */
    updateBatch(dataList: any[], extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.updateBatch, dataList, { params: extData });
    }
    /**
    * 删除一条记录
    * @param data 记录
    * @param extData 额外参数
    * @param customUrl 自定义url
    */
    delete(data: any, extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.delete, data, { params: extData });
    }
    /**
    * 删除多条记录
    * @param dataList 记录列表
    * @param extData 额外参数
    * @param customUrl 自定义url
    */
    deleteBatch(dataList: any[], extData?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.deleteBatch, dataList, { params: extData });
    }
    /**
     * 下载excel
     * @param queryParam 
     */
    downloadExcel(queryParam?: any): Promise<AxiosResponse<any>> {
        return service.post(this.baseUrl + URL_KEY.downloadExcel, queryParam, { responseType: "blob", });
    }
}


