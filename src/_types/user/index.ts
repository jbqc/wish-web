export interface LoginUser {
    /**
     * 账号
     */
    account: string,
    /**
     * 密码
     */
    password: string
}

export interface WishUser {
    userId: string;
    userName: string;
    locale: string;
    token?: string,
    menus: Array<string>,
    userAvatar: string;
}