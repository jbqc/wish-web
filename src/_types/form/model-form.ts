import { AxiosResponse } from "axios";

export enum ModelFormStatus {
    Add = 'add',
    Edit = "edit"
}

export class ModelFormOtherConfig {
    extraData?: any;//额外参数
    newModelParam?: any;//在后台获取newModel的时候携带的参数
    editPageUrl?: string;//如果配置编辑页面url 在新增完成后则会跳转
    initAdd?: Function;//可以复写 表单为新增状态的初始化方法
    initAddAfter?: Function;//initAdd执行之后的方法 如果你重写了initAdd 则不会执行这个 
    initEdit?: Function;//可以复写 表单为编辑状态的初始化方法
    initEditAfter?: Function;//initEdit执行之后的方法 如果你重写了initEdit 则不会执行这个 
}

export interface IModelFormState {
    modelFormStatus: ModelFormStatus;//特指表单状态
    add(): Promise<AxiosResponse<any>>;
    update(): Promise<AxiosResponse<any>>;
    save(): Promise<AxiosResponse<any>>;//save=add or update
}