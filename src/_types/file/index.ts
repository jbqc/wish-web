export interface FileItem {
    uid: string;
    name?: string;
    status?: string;
    response?: any;
    url?: string;
    type?: string;
    size: number;
    originFileObj: any;
}

export interface FileInfo {
    file: FileItem;
    fileList: FileItem[];
}

export type UploadChangeConfig = {
    uploading?: Function,
    done?: Function
    error?: Function
}