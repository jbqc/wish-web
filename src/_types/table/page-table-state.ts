import { AxiosResponse } from "axios";
import { PageTableHelper } from "/@/components/qc/qc-table/qc-page-table/interface";

export interface IPageTableState {
    //表格数据加载方法
    loadData(param: any): Promise<AxiosResponse<any>>,
    //搜索表格
    searchTable(): void,
    //重载表格
    reloadTable(): void,
    //获取选中的数据
    getSelectRows(): Array<any>
    //删除选中的数据
    deleteSelectData(): void
}

export interface IModelPageTableState {
    //主键
    primaryKey: string,
    //表格数据加载方法
    loadData(param: any): Promise<AxiosResponse<any>>,
    //得到表格helper,
    getTableHelper(): PageTableHelper,
    //搜索表格
    searchTable(): void,
    //重载表格
    reloadTable(): void,
    //获取选中的数据
    getSelectRows(): Array<any>
    //删除选中的数据
    deleteSelectData(): void
    //下载excel
    downloadExcel(param: any): Promise<AxiosResponse<any>>,
}