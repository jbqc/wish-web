import { createI18n, IntlDateTimeFormats } from 'vue-i18n'
import { getLocale } from '../kit/i18n-kit'
import zhCN from './langs/zh_CN'
import enUs from './langs/en_US'
import { i18nCNKey, i18nUSKey } from '/@/framework/profile';
import datetimeFormats from './datetimeFormat'
const defaultLocale = i18nCNKey;
const currentLocale = getLocale();
function getMessages(locale: string) {
    const messages = { [defaultLocale]: zhCN };
    switch (locale) {
        case i18nUSKey:
            messages[locale] = enUs;
            break;
    }
    return messages;
}

const messages = getMessages(currentLocale);
const i18n = createI18n({
    locale: currentLocale,
    legacy: false,//不使用optionapi
    fallbackLocale: defaultLocale,//如果翻译文件未找到 则显示中文文件中的值
    datetimeFormats: datetimeFormats as IntlDateTimeFormats,//日期国际化配置
    messages
})
export default i18n;
export const t = i18n.global.t;
export const d = i18n.global.d;


