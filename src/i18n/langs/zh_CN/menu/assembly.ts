export default {
    title: '组件',
    richTextEditor: {
        title: '富文本编辑器'
    },
    paginationTable: {
        title: '分页表格'
    },
    map: {
        title: '地图'
    }
}