export default {
    title: '仪表盘',
    subTitle: '仪表盘看板',
    analysis: {
        title: '分析页',
        subTitle: '仪表盘分析页',
    },
    monitor: {
        title: '监控页',
        subTitle: '仪表盘监控页',
    },
    userInfo: {
        title: '个人中心',
        subTitle: '个人中心页',
    }
}