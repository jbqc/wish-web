export default {
    title: '系统管理',
    orgAndUsers: {
        title: '组织与用户',
        listTitle: '用户列表',
        subTitle: '这是一个包含了部门管理与用户管理的组合页面',
        addTitle: '新增用户',
        editTitle: '编辑用户',
    },
    roleAndAuth: {
        title: '角色与权限',//角色与权限
        listTitle: '角色列表',//
        subTitle: '这是一个包含了角色管理与权限配置的组合页面',
        addTitle: '新增角色',
        editTitle: '编辑角色',
    },
    dictionary: {
        title: '数据字典',
        listTitle: '数据字典类型列表',
        subTitle: '这是一个包含了数据字典类型管理与数据字典子项的组合页面',
        addTitle: '新增数据字典类型',
        editTitle: '编辑数据字典类型',
    },
    menu: {
        title: '菜单管理',
        listTitle: '菜单列表',
        subTitle: '平台页面路由和后台接口管理',
    },
    appAction: {
        title: '接口管理',
        listTitle: '接口列表',
        editTitle: '编辑接口',
        subTitle: '平台后台接口管理,新增接口后分配给角色,角色就能访问当前接口',
    },
    cronTask: {
        title: '定时任务',
        listTitle: '定时任务列表',
        subTitle: '定时任务是由实现Quartz开发，支持动态新增、修改、删除、启停等功能',
    },
    loginLog: {
        title: '登录日志',
        listTitle: '登录日志列表',
        subTitle: '记录用户登录IP地址时间等',
    },
    dataSource: {
        title: '多数据源管理',
        listTitle: '多数据源列表',
        addTitle: '新增数据源',
        editTitle: '编辑数据源',
        subTitle: '可以定义多个数据源，对接其他数据库数据',
    },
    cache: {
        title: '缓存管理',
        listTitle: '缓存管理列表',
        subTitle: '配置缓存管理key，可以对缓存进行清理删除',
    },
    notice: {
        title: '通知公告',
        listTitle: '通知公告列表',
        subTitle: '配置全局通知信息',
    },
}