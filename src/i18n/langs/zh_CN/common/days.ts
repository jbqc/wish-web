export default {
    today: '今日',
    thisWeek: '本周',
    thisMonth: '本月',
    thisYear: '本年'
}