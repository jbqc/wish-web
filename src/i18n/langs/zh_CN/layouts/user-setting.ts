export default {
    logoutTitle: '注销登录？',
    logoutTips: '您真的要注销登录吗？',
    personalCenter: '个人中心',
    personalSettings: '个人设置',
    logout: '退出登录'
}