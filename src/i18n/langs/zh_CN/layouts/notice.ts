export default {
    notice: '通知',
    news: '消息',
    todoList: '待办',
    load: '加载',
    more: '更多',
    complete: '完成',
    clear: '清理',
    noticeTips: '功能还在开发完善中...请等待！'
}