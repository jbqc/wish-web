export default {
    backHome: '返回首页',
    exceptionMessage: '对不起，服务器出错了。',
    forbiddenMessage: "对不起，您无权访问此页。",
    notFoundMessage: '对不起，您访问的页面不存在。'
}