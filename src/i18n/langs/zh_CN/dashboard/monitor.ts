export default {
    gdpChartTitle: '2000-2018年各国Gdp对比',
    complteRateChartTitle: 'Starter开发完成率',
    passRateChartTitle: 'Starter考核通过率',
    completRate: '完成率',
    passRate: '通过率'
}