export default {
    userDetail: {
        label: '标签',
        design: '设计',
        development: '发展',
        marketing: '营销',
        test: '测试',
        language: '语言',
        technology: '技术',
        support: '支持',
        sales: '销售',
        ux: '用户体验',
        fraction: '分数',
        personal: '个人',
        team: '团队'
    },
    userWork: {
        article: '文章（{num}）',
        app: '应用（{num}）',
        project: '项目（{num}）',
        articleAlertMessage: '数据源来源于多数据库管理中的【WISH-BLOG】数据库',
        projectAlertMessage: '数据源来源于Gitee',
        projectCardTitle: 'Gitee项目'
    }
}