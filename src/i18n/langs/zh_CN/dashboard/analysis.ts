export default {
    totalSales: '总销售额',
    dailySales: '日销售额',
    visits: '访问量',
    dailyVisits: '日访问量',
    numberOfUsers: '用户数',
    dailyNewUsers: '日新增用户',
    numberOfOrders: '订单数',
    dailyOrders: '日订单数',
    dailyOrderCompletionRate: '日订单完成率',
    salesVolume: '销售额',
    shopAddress: '宜昌市西陵区建设路{no}号店',
    salesTrend: '销售趋势',
    trafficTrend: '访问量趋势',
    salesRanking: ' 销售额排名',
    trafficRanking: ' 访问量排名',
    indexDesc: '指标说明'
}