export default {
    onlyAccept: '你只能上传 {accept} 文件！',
    smallerSize: '文件必须小于 {size} ！'
}