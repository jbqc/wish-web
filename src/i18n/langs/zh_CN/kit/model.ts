export default {
    noSelectDeleteData: '没有选择需要删除的数据',
    deleteData: '删除数据',
    sureDeleteData: '您确定要删除选择的数据么'
}