export default {
    userTableTitle: '用户表格',
    //字段名信息
    userAccount: '用户账号',
    userName: '用户名',
    userPhone: '用户电话',
    userEmail: '用户邮箱',
    userSex: '用户性别',
    userBrithday: '用户生日',
    userIntroduce: '简介',
    //表单额外提示信息
    formExtraUserAccount: '用户账号新增后不可修改',
    //表单校验信息
    formRuleUserAccount: '请输入用户账号',
    formRuleUserAccount2: '用户账号必须为3-8位字符',
    formRuleUserName: '请输入用户名',
    //表单提示信息
    formPlhUserAccount: '取一个3到8位字符的账号吧',
    formPlhUserName: '输入您的姓名',
    formPlhUserPhone: '输入您的电话',
    formPlhUserEmail: '输入您的邮箱地址',
    formPlhUserIntroduce: '自我介绍......',
    formPlhUserSex: '请选择用户性别',
    //表单元素信息
    formBasicinfoTitle: '基本信息',
    userAvatartips: "只能上传jpg,jpeg,png文件,且大小不能超过2MB",//Only JPG, JPEG and PNG files can be uploaded, and the size cannot exceed 2MB
    userSelectTitle: '选择用户',
    notSelectedUser: '您还没有选择用户',
    useSelectModalSearchPlh: '搜索：用户账号/用户名',
}