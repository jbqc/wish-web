export default {
    selectDepartment: '选择部门',//选择部门
    parentDepartment: '父部门',//父部门
    departmentCode: '部门编码',//部门编码
    departmentName: '部门名称',//部门名称
    fullName: '全称',//全称
    departmentType: '部门类型',//部门类型
    sort: '排序',//排序
    startTime: '启用时间',//启用时间
    formPlhDeptCode: '请输入部门编码',//请输入部门编码
    formPlhDeptName: '请输入部门名称',//请输入部门名称
    formPlhStartTime: '请选择部门启用时间',//请选择部门启用时间
    formRuleDeptCode: '部门编码不能为空',//部门编码不能为空
    formRuleDeptName: '部门名称不能为空',//部门名称不能为空
    formRuleStartTime: '部门启用时间不能为空',//部门启用时间不能为空
    addFormModalTitle: '新增部门',//新增部门
    editFormModalTitle: '编辑部门',//编辑部门
    notSelectedDeptTips: '您还没有选择部门信息',
}