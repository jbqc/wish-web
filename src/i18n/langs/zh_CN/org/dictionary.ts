export default {
    dictionaryTableTitle: '数据字典项表格',
    sort: '排序',
    dictionaryCode: '字典编码',
    dictionaryName: '字典名称',
    //提示
    formPlhSortNo: '请输入排序',
    formPlhCodeId: '请输入字典项编码',
    formPlhCodeName: '请输入字典项名称',
    //校验
    formRuleSortNo: '排序为必填项',
    formRuleCodeId: '字典项编码为必填项',
    formRuleCodeName: '字典项名称为必填项',
    //表单头部
    addDictionaryTitle: '新建数据字典项',
    editDictionaryTitle: '编辑数据字典项'
}