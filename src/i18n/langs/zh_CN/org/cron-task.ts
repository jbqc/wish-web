export default {
    //列表
    tableTitle: '定时任务表格',
    addCronTaskTitle: '新建定时任务',
    editCronTaskTitle: '编辑定时任务',
    cronTaskNo: '任务编号',
    cronTaskName: '任务名称',
    active: '状态',
    cronExpression: '表达式',
    className: '执行类',
    description: '任务描述',
    operation: '操作',
    activation: '激活',
    stop: '停止',
    stopped: '停止',
    activated: '激活',
    executeOnce: '执行一次',
    changeActiveSuccessMsg: '状态修改成功！',
    runOnceJobSuccessMsg: '任务执行一次成功！',
    //提示信息
    formPlhCronTaskNo: '请输入任务编号',
    formPlhCronTaskName: '请输入任务名称',
    formPlhActive: '',//请选择任务状态
    formPlhCronExpression: '请输入任务表达式',
    formPlhClassName: '请输入任务执行类',
    formPlhdescription: '任务描述......',
    //必填校验
    formRuleCronTaskNo: '任务编号不能为空',
    formRuleCronTaskNoNumber: '任务编号只能是数字',
    formRuleCronTaskName: '任务名称不能为空',
    ormRuleActive: '',//请选择任务状态
    formRuleCronExpression: '任务表达式不能为空',
    formRuleClassName: '任务执行类不能为空',
}
