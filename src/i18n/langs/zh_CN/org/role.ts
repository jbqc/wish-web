export default {
    roleTableTitle: '角色表格',
    roleTableSerachPlh: '搜索：角色名称',
    //字段名信息
    roleName: "角色名称",
    roleDescription: "角色描述",
    roleType: '角色类型',
    roleLevel: '角色等级',
    isValid: '是否有效',
    addRole: '新增角色',
    roleInfo: '角色信息',
    roleUserOrDept: '角色（用户/部门）',
    rolePermissions: '角色权限',
    menuPermissions: '菜单权限',
    actionPermissions: '接口权限',
    //表单提示信息
    formPlhRoleName: "请输入角色名称",
    formPlhRoleDescription: "请输入角色描述",
    formPlhRoleType: "请选择角色类型",
    //表单验证
    formRuleRoleName: '角色名称不能为空',
    formRuleRoleType: '角色类型不能为空',
    addRoleBtn: '关联角色',
    effective: '有效',
    invalid: '无效'
}