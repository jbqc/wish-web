/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
export default {
    dataSourceTableTitle: '多数据源管理表格',
    dataSourceCardTitle: '多数据源管理详情',
    dataSourceCode: '数据源编码',
    dataSourceName: '数据源名称',
    dataSourceDesc: '数据源描述',
    dataSourceType: '数据源类型',
    connUrl: '连接字符串',
    status: '状态',
    userName: '用户账号',
    userPassword: '用户密码',
    sqlTemplatePath: '模板文件',
    //提示
    formPlhDataSourceCode: '请输入数据源编码',
    formPlhDataSourceName: '请输入数据源名称',
    formPlhDataSourceDesc: '请输入数据源描述',
    formPlhDataSourceType: '请输入数据源类型',
    formPlhUserName: '请输入用户账号',
    formPlhUserPassword: '请输入用户密码',
    formPlhConnUrl: '请输入连接字符串',
    formPlhStatus: '请输入状态是否生效',
    formPlhSqlTemplatePath: '请输入模板文件地址',
    //校验
    formRuleDataSourceCode: '数据源编码为必填项',
    formRuleDataSourceName: '数据源名称为必填项',
    formRuleDataSourceDesc: '数据源描述为必填项',
    formRuleDataSourceType: '数据源类型为必填项',
    formRuleUserName: '用户账号为必填项',
    formRuleUserPassword: '用户密码为必填项',
    formRuleConnUrl: '连接字符串为必填项',
    //表单头部
    addDataSourceTitle: '新建数据源',
    editDataSourceTitle: '编辑数据源',
    //搜索
    likeAll: '数据源编码/名称',
    //列表页面提示信息
    indexAlertMessage: '目前支持添加动态数据源类型：MYSQL ORACLE SQLSERVER',
}