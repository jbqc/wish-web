/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
export default {
    cacheTableTitle: '缓存管理表格',
    cacheName: '缓存名称',
    cacheExpression: '缓存表达式',
    sortNo: '排序',
    cacheId: '缓存id',
    cacheDesc: '缓存描述',
    //提示
    formPlhCacheId: '请输入缓存id',
    formPlhCacheName: '请选择缓存名称',
    formPlhCacheDesc: '请输入缓存描述',
    formPlhCacheExpression: '请输入缓存表达式',
    formPlhSortNo: '请输入排序',
    //校验
    formRuleCacheName: '缓存名称为必填项',
    formRuleCacheDesc: '缓存描述为必填项',
    formRuleCacheExpression: '缓存表达式为必填项',
    formRuleSortNo: '排序为必填项',
    //表单头部
    addCacheTitle: '新建缓存管理',
    editCacheTitle: '编辑缓存管理',
    cleanUpCacheKeyPop: '是否清理【{cacheDesc}】的所有缓存值',
    cleanUpCacheKeySuccessMessage: '【{cacheDesc}】缓存值清理成功'
}