export default {
    userGroupRoleTableTitle: '用户组角色关联表格',
    usesrRoleTableTitle: '用户角色表格',
    userOrDept: '用户/部门',
    associationType: '关联类型',
    addUserBtn: '关联用户',
    addDeptBtn: '关联部门',
    addUserAssociation: '添加用户关联',
    addDepartmentAssociation: '添加部门关联'
}