export default {
    dictionaryTypeTableTitle: '数据字典类型表格',
    sort: '排序',
    typeCode: "类型编码",
    typeName: '类型名称',
    dictionaryTypeCardTitle: '数据字典类型详情',
    addDictionaryTypeTitle: '新建数据字典类型',
    //提示
    formPlhSortNo: '请输入排序',
    formPlhCodeItemId: '请输入类型编码',
    formPlhCodeItemName: '请输入类型名称',
    //校验
    formRuleSortNo: '排序为必填项',
    formRuleCodeItemId: '类型编码为必填项',
    formRuleCodeItemName: '类型名称为必填项',
}