export default {
    departmentName: '部门名称',
    majorDepartments: '主要部门',
    isItValid: '是否有效',
    failureTime: '失效时间',
    departmentInfo: '部门信息',
}