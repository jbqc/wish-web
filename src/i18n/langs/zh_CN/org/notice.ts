/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
export default {
    noticeTableTitle: '通知表表格',
    noticeId: '通知主键',
    title: '通知标题',
    type: '通知类型',
    content: '通知类容',
    status: '状态',
    //提示
    formPlhTitle: '请输入通知标题',
    formPlhType: '请选择通知类型',
    formPlhContent: '请输入通知类容',
    formPlhStatus: '请输入状态',
    //表单验证
    formRuleTitle: '通知标题不能为空',
    formRuleType: '通知类型不能为空',
    formRuleContent: '通知内容不能为空',
    //表单头部
    addNoticeTitle: '新建通知',
    editNoticeTitle: '编辑通知',
    statusTrue: '正常',
    statusFalse: '关闭',
}