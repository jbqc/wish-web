/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
export default {
    loginLogTableTitle: '用户登录日志表格',
    loginLogTableSerachPlh: '搜索：登录名称/登录ip/登录地址',
    loginUserName: '登录名称',
    loginIp: '登录ip',
    loginArea: '登录地址',
    browser: '浏览器',
    os: '操作系统',
    createTime: '登录日期',
    //提示
    formPlhLoginUserName: '请输入登录名称',
    formPlhLoginIp: '请输入登录ip',
    formPlhLoginArea: '请输入登录地址',
    formPlhBrowser: '请输入浏览器',
    formPlhOs: '请输入操作系统',
    formPlhCreateTime: '请输入登录日期',
    //校验
    //表单头部
    addLoginLogTitle: '新建用户登录日志',
    editLoginLogTitle: '编辑用户登录日志'
}