export default {
    size: {
        default: '默认',
        medium: '中等',
        small: '紧凑'
    },
    toolbar: {
        columnDisplay: '列展示',
        columnSetting: '列设置',
        reset: '重置',
        reload: '刷新',
        density: '密度',
        downloadExcel: '下载excel',
        downloadingExcel: '正在下载excel ...',//
    },
    no: '序号',
    totalItems: '共{total}条',
    dataTable: '数据表格'
}