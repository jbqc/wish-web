export default {
    excelUpload: {
        modalTitle: 'Excel上传',
        selectFile: '选择文件',
        startUpload: '开始上传',
        uploading: '上传中',
    }
}