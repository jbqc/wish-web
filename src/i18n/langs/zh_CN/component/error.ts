export default {
    errorCode: '错误码',
    errorTime: '异常时间',
    errorInfo: '异常信息',
    errorCause: '异常原因',
    errorStack: '异常堆栈',
}