export default {
    pleaseInput: '请输入',
    pleaseSelect: '请选择',
    search: '搜索',
    reload: '重置',
    fold: '折叠',
    unfold: '展开'
}