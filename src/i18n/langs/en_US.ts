import { genMessage } from "./util";

const enModules = import.meta.globEager('./en_US/**/*.ts');
export default {
    ...genMessage(enModules, 'en_US')
}