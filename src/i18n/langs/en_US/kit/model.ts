export default {
    noSelectDeleteData: 'No data selected for deletion',//没有选择需要删除的数据
    deleteData: 'Delete data',//删除数据
    sureDeleteData: 'Are you sure you want to delete the selected data'//您确定要删除选择的数据么
}