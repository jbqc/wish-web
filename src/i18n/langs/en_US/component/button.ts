export default {
    addText: 'Add',
    cancelText: 'Cancel',
    deleteText: 'Delete',
    saveText: 'Save',
    uploadText: 'Upload'
}