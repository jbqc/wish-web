export default {
    errorCode: 'Error code',
    errorTime: 'Error time',
    errorInfo: 'Error info',
    errorCause: 'Error cause',
    errorStack: 'Error stack',
}