export default {
    size: {
        default: 'Default',
        medium: 'Medium',
        small: 'Small'
    },
    toolbar: {
        columnDisplay: 'Column display',
        columnSetting: 'Column setting',
        reset: 'Reset',
        reload: 'Reload',
        density: 'Density',
        downloadExcel: 'Download Excel',//下载excel
        downloadingExcel: 'Downloading excel ......',//正在下载excel ...
    },
    no: 'No',
    totalItems: 'total {total} items',
    dataTable: 'Data table'
}