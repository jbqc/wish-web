export default {
    pleaseInput: 'Please input',//请输入
    pleaseSelect: 'Please select',//请选择
    search: 'Search',//搜索
    reload: 'Reload',//重置
    fold: 'Fold',//折叠
    unfold: 'Unfold'//展开
}