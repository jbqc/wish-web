export default {
    excelUpload: {
        modalTitle: 'Excel Upload',//Excel上传
        selectFile: 'Select File',//选择文件
        startUpload: 'Start Upload',//开始上传
        uploading: 'Uploading',//上传中
    }
}