export default {
    totalSales: 'Total sales',
    dailySales: 'Daily sales',
    visits: 'Visits',
    dailyVisits: 'Daily visits',
    numberOfUsers: 'Number of users',
    dailyNewUsers: 'Daily new users',
    numberOfOrders: 'Number of orders',
    dailyOrders: 'Daily orders',
    dailyOrderCompletionRate: 'Daily order completion rate',
    salesVolume: 'Sales volume',
    shopAddress: 'No.{no} shop, Jianshe Road, Xiling District, Yichang City',
    salesTrend: 'Sales trend',
    trafficTrend: 'Traffic Trend',
    salesRanking: 'Sales ranking',
    trafficRanking: 'Traffic ranking',
    indexDesc: 'Index description'
}