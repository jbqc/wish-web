export default {
    gdpChartTitle: 'Comparison of GDP in different countries from 2000 to 2018',
    complteRateChartTitle: 'Starter development completion rate',
    passRateChartTitle: 'Starter pass rate',
    completRate: 'Completion rate',
    passRate: 'percent of pass'
}