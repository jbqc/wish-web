export default {
    userDetail: {
        label: 'label',
        design: 'Design',//设计
        development: 'Development',//发展
        marketing: 'Marketing',//营销
        test: 'Test',//测试
        language: 'Language',//语言
        technology: 'Technology',//技术
        support: 'Support',//支持
        sales: 'Sales',//销售
        ux: 'UX',//用户体验
        fraction: 'fraction',//分数
        personal: 'personal',//个人
        team: 'team',//团队
    },
    userWork: {
        article: 'Article ({num})',
        app: 'Application ({num})',
        project: 'Project ({num})',
        articleAlertMessage: 'The data source comes from the 【WISH-BLOG】 database in multi database management',
        projectAlertMessage: 'The data source is Gitee',
        projectCardTitle: 'Gitee project'
    }
}