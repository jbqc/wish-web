export default {
    logoutTitle: 'Log out?',//注销登录？
    logoutTips: 'Do you really want to log out?',//您真的要注销登录吗？
    personalCenter: 'Personal Center',//个人中心
    personalSettings: 'Personal settings',//个人设置
    logout: 'Log out'//退出登录
}