export default {
    notice: 'Notice',//通知
    news: 'News',//消息
    todoList: 'To do',//待办
    load: 'Load',//加载
    more: 'More',//更多
    complete: 'Complete',//完成
    clear: 'Clear',//清理
    noticeTips: 'The function is still under development... Please wait!'
}