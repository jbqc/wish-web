export default {
    title: 'Assembly',
    richTextEditor: {
        title: 'Rich Text Editor'
    },
    paginationTable: {
        title: 'Pagination table'
    },
    map: {
        title: 'Map'
    }
}