export default {
    title: 'Dashboard',
    subTitle: 'Dashboard Kanban',
    analysis: {
        title: 'Analysis page',
        subTitle: 'Dashboard Analysis page',
    },
    monitor: {
        title: 'Monitoring page',
        subTitle: 'Dashboard monitoring page',
    },
    userInfo: {
        title: 'Personal Center',//个人中心
        subTitle: 'Personal Center page',//个人中心页
    }
}