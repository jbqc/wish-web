export default {
    title: 'System management',
    orgAndUsers: {
        title: 'Org and users',
        listTitle: 'User table',//用户列表
        subTitle: 'This is a combination of department management and user management page',
        addTitle: 'New user',//新增用户
        editTitle: 'Edit user',//编辑用户
    },
    roleAndAuth: {
        title: 'Roles and auth',//角色与权限(英文太长不好看)
        listTitle: 'Role table',//角色列表
        subTitle: 'This is a combination page including role management and permission configuration',
        addTitle: 'New role',//新增角色
        editTitle: 'Edit role',//编辑角色
    },
    dictionary: {
        title: 'Data dictionary',//数据字典
        listTitle: 'Data dictionary type table',//数据字典类型列表
        subTitle: 'This is a combination page containing data dictionary type management and data dictionary subitems',
        addTitle: 'New data dictionary',//新增数据字典类型
        editTitle: 'Edit data dictionary',//编辑数据字典类型
    },
    menu: {
        title: 'Menu management',//菜单管理
        listTitle: 'Menu table',//菜单列表
        subTitle: 'Platform page routing and background interface management',//平台页面路由和后台接口管理
    },
    appAction: {
        title: 'Interface manage',//接口管理
        listTitle: 'Interface list',//接口列表
        editTitle: 'Edit interface',//编辑接口
        subTitle: 'Platform background interface management, new interface assigned to the role, the role can access the current interface',//平台后台接口管理,新增接口后分配给角色,角色就能访问当前接口
    },
    cronTask: {
        title: 'Scheduled task',//定时任务
        listTitle: 'Scheduled task list',//定时任务列表
        subTitle: 'Scheduled task is developed by quartz, which supports dynamic addition, modification, deletion, start stop and other functions',//定时任务是由实现Quartz开发，支持动态新增、修改、删除、启停等功能
    },
    loginLog: {
        title: 'Log in',//登录日志
        listTitle: 'Log list',//登录日志列表
        subTitle: 'Record the time of user login IP address, etc',//记录用户登录IP地址时间等
    },
    dataSource: {
        title: 'Multi data source management',//多数据源管理
        listTitle: 'Multi data source list',//多数据源列表
        addTitle: 'Add data source',//新增数据源
        editTitle: 'Edit data source',//编辑数据源
        subTitle: 'Multiple data sources can be defined to connect with other database data',//可以定义多个数据源，对接其他数据库数据
    },
    cache: {
        title: 'Cache management',//缓存管理
        listTitle: 'Cache management list',//缓存管理列表
        subTitle: 'Configure the cache management key to clean and delete the cache',//配置缓存管理key，可以对缓存进行清理删除
    },
    notice: {
        title: 'Notice announcement',
        listTitle: 'Notice announcement list',
        subTitle: 'Configure global notification information',
    },
}