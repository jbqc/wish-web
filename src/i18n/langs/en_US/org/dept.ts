export default {
    selectDepartment: 'Select Department',//选择部门
    parentDepartment: 'Parent department',//父部门
    departmentCode: 'Department code',//部门编码
    departmentName: 'Department name',//部门名称
    fullName: 'Full name',//全称
    departmentType: 'Department type',//部门类型
    sort: 'sort',//排序
    startTime: 'Start time',//启用时间
    formPlhDeptCode: 'Please input department code',//请输入部门编码
    formPlhDeptName: 'Please input department name',//请输入部门名称
    formPlhStartTime: 'Please select the opening time of the Department',//请选择部门启用时间
    formRuleDeptCode: 'Department code cannot be empty',//部门编码不能为空
    formRuleDeptName: 'Department name cannot be empty',//部门名称不能为空
    formRuleStartTime: 'Department activation time cannot be empty',//部门启用时间不能为空
    addFormModalTitle: 'New Department',//新增部门
    editFormModalTitle: 'Editorial department',//编辑部门
    notSelectedDeptTips: 'You have not selected department information',//您还没有选择部门信息

}