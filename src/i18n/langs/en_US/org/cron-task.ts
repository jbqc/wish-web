export default {
    //列表
    tableTitle: 'Scheduled task table',//定时任务表格
    addCronTaskTitle: 'New scheduled task',//新建定时任务
    editCronTaskTitle: 'Edit scheduled task',//编辑定时任务
    cronTaskNo: 'Task no',//任务编号
    cronTaskName: 'Task name',//任务名称
    active: 'state',//状态
    cronExpression: 'Cron expression',//表达式
    className: 'Execution class',//执行类
    description: 'Task description',//任务描述
    operation: 'operation',//操作
    activation: 'activation',//激活
    stop: 'stop',//停止
    stopped: 'Stopped',//停止
    activated: 'Activated',//激活
    executeOnce: 'execute once',//执行一次
    changeActiveSuccessMsg: 'Status modification succeeded!',//状态修改成功！
    runOnceJobSuccessMsg: 'The task was successfully executed once!',//任务执行一次成功！
    //提示信息
    formPlhCronTaskNo: 'Please enter the task no',//请输入任务编号
    formPlhCronTaskName: 'Please enter the task name',//请输入任务名称
    //formPlhActive: '',//请选择任务状态
    formPlhCronExpression: 'Please enter the cron expression',//请输入任务表达式
    formPlhClassName: 'Please enter the execution class',//请输入任务执行类
    formPlhdescription: 'Please enter the task description',//任务描述......
    //必填校验
    formRuleCronTaskNo: 'task no cannot be empty',//任务编号不能为空
    formRuleCronTaskNoNumber: 'Task no can only be numeric',//任务编号只能是数字
    formRuleCronTaskName: 'task name cannot be empty',//任务名称不能为空
    //ormRuleActive: '',//请选择任务状态
    formRuleCronExpression: 'task cron expression cannot be empty',//任务表达式不能为空
    formRuleClassName: 'task execution class cannot be empty',//任务执行类不能为空
}