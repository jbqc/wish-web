export default {
    roleTableTitle: 'Role Table',//角色表格
    roleTableSerachPlh: 'Search: role name',//搜索：角色名称
    //字段名信息
    roleName: "Role name",//角色名称
    roleDescription: "Role description",//角色描述
    roleType: 'Role type',//角色类型
    roleLevel: 'Role level',//角色等级
    isValid: 'Is it valid',//是否有效
    addRole: 'New role',//新增角色
    roleInfo: 'Role information',//角色信息
    roleUserOrDept: 'Role (user / Department)',//角色（用户/部门）
    rolePermissions: 'Role permissions',//角色权限
    menuPermissions: 'Menu permissions',//菜单权限
    actionPermissions: 'Interface permissions',//接口权限
    //表单提示
    formPlhRoleName: "Please enter the role name",//请输入角色名称
    formPlhRoleDescription: "Please enter the role description",//请输入角色描述
    formPlhRoleType: "Please select role type",//请选择角色类型
    //表单验证
    formRuleRoleName: 'Role name cannot be empty',//角色名称不能为空
    formRuleRoleType: 'Role type cannot be empty',//角色类型不能为空
    addRoleBtn: 'Associated roles',//关联角色
    effective: 'effective',//有效的
    invalid: 'invalid'//无效的
}