export default {
    dictionaryTypeTableTitle: 'Data dictionary type table',//数据字典类型表格
    sort: 'Sort',//排序
    typeCode: "Type code",//类型编码
    typeName: 'Type name',//类型名称,
    dictionaryTypeCardTitle: 'Data dictionary type details',//数据字典类型详情
    addDictionaryTypeTitle: 'New data dictionary type',//新建数据字典类型
    //提示
    formPlhSortNo: 'Please enter sort',//请输入排序
    formPlhCodeItemId: 'Please enter type code',//请输入类型编码
    formPlhCodeItemName: 'Please enter the type name',//请输入类型名称
    //校验
    formRuleSortNo: 'Sorting is required',//排序为必填项
    formRuleCodeItemId: 'Type code is required',//类型编码为必填项
    formRuleCodeItemName: 'Type name is required',//类型名称为必填项
}