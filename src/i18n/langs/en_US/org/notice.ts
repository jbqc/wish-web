/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
export default {
    noticeTableTitle: 'Notification table',
    title: 'Notice title',
    type: 'Notification type',
    content: 'Notification content',
    status: 'state',
    //提示
    formPlhTitle: 'Please enter a notification title',
    formPlhType: 'Please select a notification type',
    formPlhContent: 'Please enter the notification content',
    formPlhStatus: 'Please enter the status',
    //表单验证
    formRuleTitle: 'Notification Title cannot be empty',
    formRuleType: 'Notification type cannot be empty',
    formRuleContent: 'Notification content cannot be empty',
    //表单头部
    addNoticeTitle: 'New notification',
    editNoticeTitle: 'Edit notification',
    statusTrue: 'normal',
    statusFalse: 'close',
}