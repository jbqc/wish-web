/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
export default {
    dataSourceTableTitle: 'Multi data source management table',//多数据源管理表格
    dataSourceCardTitle: 'Multi data source management details',//多数据源管理详情
    dataSourceCode: 'data source code',//数据源编码
    dataSourceName: 'data source name',//数据源名称
    dataSourceDesc: 'data source description',//数据源描述
    dataSourceType: 'data source type',//数据源类型
    connUrl: 'connection string',//连接字符串
    status: 'status',//状态
    userName: 'user account',//用户账号
    userPassword: 'user password',//用户密码
    sqlTemplatePath: 'template path',//模板文件
    //提示
    formPlhDataSourceCode: 'Please enter data source code',//请输入数据源编码
    formPlhDataSourceName: 'Please enter data source name',//请输入数据源名称
    formPlhDataSourceDesc: 'Please enter data source description',//请输入数据源描述
    formPlhDataSourceType: 'Please enter data source type',//请输入数据源类型
    formPlhUserName: 'Please enter user account',//请输入用户账号
    formPlhUserPassword: 'Please enter user password',//请输入用户密码
    formPlhConnUrl: 'Please enter connection string',//请输入连接字符串
    //formPlhStatus: 'Please enter data source code',//请输入状态是否生效
    formPlhSqlTemplatePath: 'Please enter sql template file path',//请输入模板文件地址
    //校验
    formRuleDataSourceCode: 'data source code is required',//数据源编码为必填项
    formRuleDataSourceName: 'data source name is required',//数据源名称为必填项
    formRuleDataSourceDesc: 'data source description is required',//数据源描述为必填项
    formRuleDataSourceType: 'data source type is required',//数据源类型为必填项
    formRuleUserName: 'user account is required',//用户账号为必填项
    formRuleUserPassword: 'user password is required',//用户密码为必填项
    formRuleConnUrl: 'connection string is required',//连接字符串为必填项
    //表单头部
    addDataSourceTitle: 'New data source',//新建数据源
    editDataSourceTitle: 'Edit data source',//编辑数据源
    //搜索
    likeAll: 'data source code/name',//数据源编码/名称
    //列表页面提示信息
    indexAlertMessage: 'Currently, it supports adding dynamic data source types:MYSQL ORACLE SQLSERVER',
}