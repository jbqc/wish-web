export default {
    departmentName: 'Department name',//部门名称
    majorDepartments: 'Major departments',//主要部门
    isItValid: 'Is it valid',//是否有效
    failureTime: 'Failure time',//失效时间
    departmentInfo: 'Department information',//部门信息
}