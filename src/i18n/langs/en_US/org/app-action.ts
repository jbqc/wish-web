export default {
    appActionTableTitle: 'Interface table',//接口表格
    addAppActionTitle: 'New interface',//新建接口
    editAppActionTitle: 'Edit interface',//编辑接口
    appActionInfo: 'Interface information',//接口信息
    actionName: 'Interface name',//接口名称
    sortNo: 'sort',//排序
    actionPath: 'Interface path',//接口地址
    actionType: 'Interface type',//接口类型
    authorityKey: 'Permission key',//权限键
    //表单部分
    formRuleSortNo: 'Sorting is required',//排序为必填项
    formRuleActionName: 'Interface name is required',//接口名称为必填项
    formRuleActionPath: 'Interface path is required',//接口地址为必填项
    formRuleActionType: 'Interface type is required',//接口类型为必填项
    formRuleAuthorityKey: 'Interface permission key is required',//接口权限为必填项
    formPlhActionName: 'Please enter the interface name',//请输入接口名称
    formPlhActionPath: 'Please input the interface path',//请输入接口地址
    formPlhSortNo: 'Please enter sort',//请输入排序
    formPlhAuthorityKey: 'Please enter the permission key',//请输入权限键
    formPlhActionType: 'Please select interface type',//请选择接口类型,
    initBtn: 'initialization',//初始化
}