export default {
    dictionaryTableTitle: 'Data dictionary entry table',//数据字典项表格
    sort: 'Sort',//排序
    dictionaryCode: 'Dictionary code',//字典编码
    dictionaryName: 'Dictionary name',//字典名称
    //提示
    formPlhSortNo: 'Please enter sort',//请输入排序
    formPlhCodeId: 'Please enter type code',//请输入字典项编码
    formPlhCodeName: 'Please enter the type name',//请输入字典项名称
    //校验
    formRuleSortNo: 'Sorting is required',//排序为必填项
    formRuleCodeId: 'Type code is required',//字典项编码为必填项
    formRuleCodeName: 'Type name is required',//字典项名称为必填项
    addDictionaryTitle: 'New data dictionary entry',//新建数据字典项
    editDictionaryTitle: 'Edit data dictionary entry'//编辑数据字典项
}