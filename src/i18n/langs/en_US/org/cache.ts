/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
export default {
    cacheTableTitle: 'Cache management table',//缓存管理表格
    cacheName: 'Cache name',//缓存名称
    cacheExpression: 'Cache expression',//缓存表达式
    sortNo: 'sort no',//排序
    cacheDesc: 'Cache describe',//缓存键值
    //提示
    formPlhCacheName: 'Please select cache name',//请选择缓存名称
    formPlhCacheDesc: 'Please enter the cache describe',//请输入缓存键值
    formPlhCacheExpression: 'Please enter a cache expression',//请输入缓存表达式
    formPlhSortNo: 'Please input sort',// 请输入排序
    //校验
    formRuleCacheName: 'Cache name is required',//缓存名称为必填项
    formRuleCacheDesc: 'Cache describe is required',//缓存键值为必填项
    formRuleCacheExpression: 'Cache expression is required',//缓存表达式为必填项
    formRuleSortNo: 'Sorting is required',//排序为必填项
    //表单头部
    addCacheTitle: 'New cache management',//新建缓存管理
    editCacheTitle: 'Edit cache management',//编辑缓存管理
    cleanUpCacheKeyPop: 'Do you want to clear all cache values of [{cacheDesc}]',//是否清理【{0}】的所有缓存值
    cleanUpCacheKeySuccessMessage: '{cachedesc}] cache value cleaned successfully',//【{cacheDesc}】缓存值清理成功
}