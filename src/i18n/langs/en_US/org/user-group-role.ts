export default {
    userGroupRoleTableTitle: 'User group role association table',//用户组角色关联表格
    usesrRoleTableTitle: 'User role table',//用户角色表格
    userOrDept: 'User / Department',//用户/部门
    associationType: 'Association type',//关联类型
    associatedUsers: 'Associated users',//关联用户
    addUserBtn: 'Add User',//关联用户
    addDeptBtn: 'Add Department',//关联部门
    addUserAssociation: 'Add User Association',//关联用户
    addDepartmentAssociation: 'Add Department Association'//关联部门
}