/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
export default {
    loginLogTableTitle: 'User login log table',//用户登录日志表格
    loginLogTableSerachPlh: 'Search: login name / login IP / login address',//搜索：登录名称/登录ip/登录地址
    loginUserName: 'login name',//登录名称
    loginIp: 'login IP',//登录ip
    loginArea: 'login address',//登录地址
    browser: 'browser',//浏览器
    os: 'os',//操作系统
    createTime: 'login date',//
    //提示
    formPlhLoginUserName: 'Please enter login name',//请输入登录名称
    formPlhLoginIp: 'Please enter login ip',//请输入登录ip
    formPlhLoginArea: 'Please enter login address',//请输入登录地址
    formPlhBrowser: 'Please enter browser',//请输入浏览器
    formPlhOs: 'Please enter login os',//请输入操作系统
    formPlhCreateTime: 'Please enter login date',//请输入登录日期
    //校验
    //表单头部
    addLoginLogTitle: 'New user login log',//新建用户登录日志
    editLoginLogTitle: 'Edit user login log'//编辑用户登录日志
}