export default {
    userTableTitle: 'User Table',//用户表格
    //字段名信息
    userAccount: 'User account',//用户账号
    userName: 'User name',//用户名
    userPhone: 'User phone',//用户电话
    userEmail: 'User email',//用户邮箱
    userSex: 'User sex',//用户性别
    userBrithday: 'User brithday',//用户生日
    userIntroduce: "Introduce",//简介
    //表单额外提示信息
    formExtraUserAccount: 'The user account cannot be modified after it is added',//用户账号新增后不可修改
    //表单校验信息
    formRuleUserAccount: 'Please input user account',//请输入用户账号
    formRuleUserAccount2: 'User account number must be 3-8 characters',//用户账号必须为3-8位字符
    formRuleUserName: 'Please enter the user name',//请输入用户名
    //表单提示信息
    formPlhUserAccount: 'Take a three to eight character account',//取一个3到8位字符的账号吧
    formPlhUserName: 'Enter your name',//输入您的姓名
    formPlhUserPhone: 'Enter your phone number',//输入您的电话
    formPlhUserEmail: 'Enter your email address',//输入您的邮箱地址
    formPlhUserIntroduce: 'introduce oneself to......',//自我介绍......
    formPlhUserSex: 'Please select user gender',//请选择用户性别
    //表单元素信息
    formBasicinfoTitle: 'Basic information',//基本信息
    userAvatartips: "Only JPG, JPEG and PNG files can be uploaded, and the size cannot exceed 2MB",//只能上传jpg,jpeg,png文件,且大小不能超过2MB
    userSelectTitle: 'Select user',//选择用户
    notSelectedUser: 'You have not selected a user',//您还没有选择用户
    useSelectModalSearchPlh: 'Search: user account / user name',//搜索：用户账号/用户名
}