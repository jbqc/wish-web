export default {
    today: 'today',
    thisWeek: 'This week',
    thisMonth: 'This month',
    thisYear: 'This year'
}