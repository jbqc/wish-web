export default {
    backHome: 'Back to home page',//返回首页
    exceptionMessage: 'Sorry, there was an error on the server.',//对不起，服务器出错了。
    forbiddenMessage: "Sorry, you don't have access to this page.",//对不起，您无权访问此页。
    notFoundMessage: 'Sorry, the page you visited does not exist.'//对不起，您访问的页面不存在。
}