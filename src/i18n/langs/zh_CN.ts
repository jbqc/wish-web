import { genMessage } from "./util";

const zhCNModules = import.meta.globEager('./zh_CN/**/*.ts');
export default {
    ...genMessage(zhCNModules, 'zh_CN')
}