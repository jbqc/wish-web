/*
 * @Descripttion: 初始化方法
 * @version: 0.0.1
 * @Author: zhu_liangyu
 * @Date: 2020-11-11 15:28:20
 * @LastEditors: zhu_liangyu
 * @LastEditTime: 2020-11-11 16:10:19
 */
import { App } from "vue";
import antdComponents from "/@/framework/init/antd";
import qcComponents from "/@/framework/init/qc";

import wishDirectiveList, { WishDirective } from '/@/framework/init/directive'

/**
 * 初始化入口方法
 * @param app 
 */
export function init(app: App) {
    initAntdComponents(app);
    initQcComponents(app);
    initWishDirective(app);
}

/**
 * 初始化注入antd常用组件
 * @param app 
 */
function initAntdComponents(app: App) {
    for (let i in antdComponents) {
        app.use(antdComponents[i]);
    }
}

/**
 * 初始化全局组件
 * @param app 
 */
function initQcComponents(app: App) {
    for (let i in qcComponents) {
        app.use(qcComponents[i]);
    }
}

/**
 * 初始化全局指令
 * @param app 
 */
function initWishDirective(app: App) {
    for (let i in wishDirectiveList) {
        const wishDirective: WishDirective = wishDirectiveList[i];
        app.directive(wishDirective.name, wishDirective.directive);
    }
}
