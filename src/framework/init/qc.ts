import { QcAddButton, QcAsyncTree, QcBreadcrumb, QcBasicModal, QcCancelButton, QcDeleteButton, QcDictionarySelect, QcExcelUpload, QcLink, QcIcon, QcJsonPretty, QcPageHeader, QcPageTable, QcSaveButton, QcQueryForm, QcUploadButton } from '/@/components/qc/index'

const qcComponents = [
    QcAddButton,
    QcAsyncTree,
    QcBasicModal,
    QcBreadcrumb,
    QcCancelButton,
    QcDeleteButton,
    QcDictionarySelect,
    QcExcelUpload,
    QcLink,
    QcIcon,
    QcJsonPretty,
    QcPageHeader,
    QcPageTable,
    QcSaveButton,
    QcQueryForm,
    QcUploadButton
];
export default qcComponents;