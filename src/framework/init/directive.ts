import { App, Directive } from "vue";

export type WishDirective = {
    name: string,
    directive: Directive
}

/**
 * 在手机端隐藏这个东西
 */
const hideMobileDirective: Directive = {
    mounted: (el: HTMLElement) => {
        const flag = navigator.userAgent.match(
            /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
        );
        if (flag) el.style.display = "none"
    }
}
const wishDirectiveList: Array<WishDirective> = [
    { name: 'hideMobile', directive: hideMobileDirective }
]

export default wishDirectiveList