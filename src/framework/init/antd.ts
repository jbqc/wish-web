/*
 * @Descripttion:
 * @version: 0.0.1
 * @Author: zhu_liangyu
 * @Date: 2020-11-11 15:31:57
 * @LastEditors: zhu_liangyu
 * @LastEditTime: 2020-11-11 15:41:45
 */
import {
    Alert,
    Avatar,
    Badge,
    Breadcrumb,
    Button,
    Card,
    Checkbox,
    Col,
    DatePicker,
    Divider,
    Dropdown,
    Form,
    Input,
    InputNumber,
    Layout,
    Menu,
    Modal,
    PageHeader,
    Popconfirm,
    Popover,
    Progress,
    Rate,
    Row,
    Radio,
    Select,
    Space,
    Switch,
    Table,
    Tabs,
    Tag,
    TimePicker,
    Timeline,
    Tooltip,
    Tree,
    Upload
} from 'ant-design-vue'

const antdComponents = [
    Alert,
    Avatar,
    Badge,
    Breadcrumb,
    Button,
    Card,
    Checkbox,
    Col,
    DatePicker,
    Divider,
    Dropdown,
    Form,
    Input,
    InputNumber,
    Layout,
    Menu,
    Modal,
    PageHeader,
    Popconfirm,
    Popover,
    Progress,
    Rate,
    Radio,
    Row,
    Select,
    Space,
    Switch,
    Table,
    Tabs,
    Tag,
    TimePicker,
    Timeline,
    Tooltip,
    Tree,
    Upload
];

export default antdComponents;