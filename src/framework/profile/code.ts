// LOGIN_SUCCESS(10001, "登录成功"),

// LOGIN_ERROR(10002, "账号或者密码错误"),

// ERROR_AUTHORITY(10003, "资源不可用,请联系管理员设置权限"),

// LOGIN_MORE(10004, "您已在别处登录,请注意密码信息是否泄露"),

// LOGOUT(10005, "当前登录已失效,请重新登录"),

// CAPTCHA_ERROR(10006, "验证码错误"),

// ERROR(50000, "系统异常,请联系管理员"),
/**
 * 资源不可用,请联系管理员设置权限
 */
export const ERROR_AUTHORITY_CODE = 10003;
/**
 * 当前登录已失效,请重新登录
 */
export const LOGOUT_CODE = 10005;
/**
 * 系统异常,请联系管理员
 */
export const ERROR_CODE = 50000;

export const SUCCESS_CODE = 60000;

/**
 * 定义成功状态
 */
export const successCodeArr = [10001, SUCCESS_CODE, 60001, 60002, 60003];

/**
 * 定义需要提示的数组
 */
export const messageSuccessArr = [60001, 60002, 60003];