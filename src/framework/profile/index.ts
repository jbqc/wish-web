/**
 * 白名单菜单路由
 */
export const whiteList = ['/login'];

/**
 * 项目根路径
 */
export const baseUrl = '/';

export const i18nCNKey = 'zh-CN'

export const i18nUSKey = 'en-US'
