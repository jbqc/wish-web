import { t } from '/@/i18n/index'
export const appActionTableColumns = [
    { title: t('org.app-action.sortNo'), key: "sortNo", dataIndex: "sortNo", sorter: true },
    { title: t('org.app-action.actionName'), key: "actionName", dataIndex: "actionName", slots: { customRender: 'customActionName' } },
    { title: t('org.app-action.actionType'), key: "actionType", dataIndex: "actionType", slots: { customRender: 'customActionType' } },
    { title: t('org.app-action.authorityKey'), key: "authorityKey", dataIndex: "authorityKey" },
];

export const appActionFormRule = {
    sortNo: [{ type: 'number', required: true, message: t('org.app-action.formRuleSortNo') }],
    actionName: [{ required: true, message: t('org.app-action.formRuleActionName') }],
    authorityKey: [{ required: true, message: t('org.app-action.formRuleAuthorityKey') }],
}

export const appActionQueryColumns = [
    { type: "input", name: "actionName", label: t('org.app-action.actionName') },
    { type: "input", name: "authorityKeyLike", label: t('org.app-action.authorityKey') }
]

/**
 * 数据字典 接口类型
 */
export const AppActionTypeDic = "ORG_APP_ACTION_TYPE";
/**
 * 数据字典 接口类型 接口类
 */
export const AppActionTypeDic_C = "Controller";
/**
 * 数据字典 接口类型 接口方法
 */
export const AppActionTypeDic_F = "Function";

export const appActionIndexPageUrl = "/org/app-action/index";

export const appActionEditPageUrl = "/org/app-action/edit/";