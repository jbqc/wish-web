/**
 * @author light_dust_generator
 * @since 2021-06-23 19:12:53
 */
import { t } from '/@/i18n/index'

export const loginLogTableColumns = [
    { title: t('org.login-log.loginUserName'), key: "loginUserName", dataIndex: "loginUserName" },
    { title: t('org.login-log.loginIp'), key: "loginIp", dataIndex: "loginIp", },
    { title: t('org.login-log.loginArea'), key: "loginArea", dataIndex: "loginArea" },
    { title: t('org.login-log.browser'), key: "browser", dataIndex: "browser" },
    { title: t('org.login-log.os'), key: "os", dataIndex: "os" },
    { title: t('org.login-log.createTime'), key: "createTime", dataIndex: "createTime", sorter: true, },
]

export const loginLogQueryColumns = [
]

export const loginLogFormRule = {
}

export const loginLogIndexPageUrl = "/org/login-log/index";
export const loginLogAddPageUrl = "/org/login-log/add";
export const loginLogEditUrl = "/org/login-log/edit/";