import { t } from '/@/i18n/index'
export const roleTableColumns = [
    { title: t('org.role.roleName'), key: "roleName", dataIndex: "roleName", slots: { customRender: 'customRoleName' } },
    { title: t('org.role.roleType'), key: "roleTypeDesc", dataIndex: "roleTypeDesc", slots: { customRender: 'customRoleType' } },
    {
        title: t('org.role.isValid'), key: "isValid", dataIndex: "isValid", slots: { customRender: 'customIsValid' },
        filters: [
            {
                text: t('org.role.effective'),
                value: 1,
            },
            {
                text: t('org.role.invalid'),
                value: 0,
            },
        ],
        filterMultiple: false,

    },
    { title: t('org.role.roleDescription'), key: "roleDescription", dataIndex: "roleDescription", ellipsis: true },
];

export const roleFormRule = {
    roleName: [{ required: true, message: t('org.role.formRuleRoleName') }],
    roleType: [{ required: true, message: t('org.role.formRuleRoleType'), trigger: 'blur' }],
}

//链接地址定义
export const roleIndexPageUrl = "/org/role/index"
export const roleAddPageUrl = "/org/role/add";
export const roleEditPageUrl = "/org/role/edit/";