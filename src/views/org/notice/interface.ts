/**
 * @author light_dust_generator
 * @since 2021-08-23 14:21:49
 */
import { t } from '/@/i18n/index'

export const noticeTableColumns = [
    { title: t('org.notice.title'), key: "title", dataIndex: "title", slots: { customRender: "customNoticeId" } },
    { title: t('org.notice.type'), key: "type", dataIndex: "type", slots: { customRender: "customType" } },
    { title: t('org.notice.content'), key: "content", dataIndex: "content" },
    {
        title: t('org.notice.status'), key: "status", dataIndex: "status", slots: { customRender: "customStatus" }, filters: [
            {
                text: t('org.notice.statusTrue'),
                value: 1,
            },
            {
                text: t('org.notice.statusFalse'),
                value: 0,
            },
        ],
        filterMultiple: false,
    },
]

export const noticeQueryColumns = [
    { type: "input", name: "title", label: t('org.notice.title'), },
]

export const noticeFormRule = {
    title: [{ required: true, message: t('org.notice.formRuleTitle') }],
    type: [{ required: true, message: t('org.notice.formRuleType') }],
    content: [{ required: true, message: t('org.notice.formRuleContent') }],
}

export const noticeIndexPageUrl = "/org/notice/index";
export const noticeAddPageUrl = "/org/notice/add";
export const noticeEditUrl = "/org/notice/edit/";