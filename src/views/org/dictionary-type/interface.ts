import { t } from '/@/i18n/index'
export const dictionaryTypeTableColumns = [
    { title: t('org.dictionary-type.sort'), key: "sortNo", dataIndex: "sortNo", sorter: true },
    { title: t('org.dictionary-type.typeCode'), key: "codeItemId", dataIndex: "codeItemId", slots: { customRender: 'customCodeItemId' } },
    { title: t('org.dictionary-type.typeName'), key: "codeItemName", dataIndex: "codeItemName" },
];

export const dictionaryTypeQueryColumns = [
    { type: "input", name: "codeItemId", label: t('org.dictionary-type.typeCode') },
    { type: "input", name: "codeItemName", label: t('org.dictionary-type.typeName') }
]

export const dictionaryTypeFormRule = {
    sortNo: [{ type: 'number', required: true, message: t('org.dictionary-type.formRuleSortNo') }],
    codeItemId: [{ required: true, message: t('org.dictionary-type.formRuleCodeItemId') }],
    codeItemName: [{ required: true, message: t('org.dictionary-type.formRuleCodeItemName') }],
}

export const dictionaryIndexPageUrl = "/org/dictionary/index";
export const dictionaryEditPageUrl = "/org/dictionary/edit/";