import { t } from '/@/i18n/index'
export const userDeptTableColumns = [
    {
        title: t('org.user-dept.departmentName'),
        key: "deptName",
        dataIndex: "deptName",
        sorter: true,
    },
    { title: t('org.user-dept.majorDepartments'), key: "main", dataIndex: "main", slots: { customRender: 'customMain' } },
    { title: t('org.user-dept.isItValid'), key: "valid", dataIndex: "valid", slots: { customRender: 'customValid' } },
    { title: t('org.user-dept.failureTime'), key: "timeEnd", dataIndex: "timeEnd", slots: { customRender: 'customTimeEnd' } }
];