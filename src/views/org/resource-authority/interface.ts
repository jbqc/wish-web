/**
 * 资源类型
 */
export const RESOURCE_TYPE = {
    /**
     * 接口
     */
    INTERFACE: 'INTERFACE',
    /**
     * 菜单
     */
    MENU: 'MENU'
}

/**
 * 资源分类
 */
export const RESOURCE_CATEGORY = {
    /**
     * 可使用
     */
    USE: 'USE',
    /**
     * 可分配
     */
    ASSIGN: 'ASSIGN'
}