import { t } from '/@/i18n/index'
export const dictionaryTableColumns = [
    { title: t('org.dictionary.sort'), key: "sortNo", dataIndex: "sortNo", sorter: true },
    { title: t('org.dictionary.dictionaryCode'), key: "codeId", dataIndex: "codeId", slots: { customRender: 'customCodeId' } },
    { title: t('org.dictionary.dictionaryName'), key: "codeName", dataIndex: "codeName" },
];

export const dictionaryFormRule = {
    sortNo: [{ type: 'number', required: true, message: t('org.dictionary.formRuleSortNo') }],
    codeId: [{ required: true, message: t('org.dictionary.formRuleCodeId') }],
    codeName: [{ required: true, message: t('org.dictionary.formRuleCodeName') }],
}