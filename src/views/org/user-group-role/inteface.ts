import { t } from '/@/i18n/index'
export const roleUserGorupTableColumns = [
    { title: t('org.user-group-role.userOrDept'), key: "userGroupName", dataIndex: "userGroupName" },
    { title: t('org.user-group-role.associationType'), key: "userGroupTypeDesc", dataIndex: "userGroupTypeDesc" },
]