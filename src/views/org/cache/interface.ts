/**
 * @author light_dust_generator
 * @since 2021-07-10 19:39:32
 */
import { t } from '/@/i18n/index'

export const cacheTableColumns = [
    { title: t('org.cache.sortNo'), key: "sortNo", dataIndex: "sortNo" },
    { title: t('org.cache.cacheDesc'), key: "cacheDesc", dataIndex: "cacheDesc", slots: { customRender: "customCacheDesc" } },
    { title: t('org.cache.cacheName'), key: "cacheName", dataIndex: "cacheName" },
    { title: t('org.cache.cacheExpression'), key: "cacheExpression", dataIndex: "cacheExpression" },
    { title: t('common.whole.operation'), key: "action", slots: { customRender: "customAction" } },
]

export const cacheQueryColumns = [
]

export const cacheFormRule = {
    cacheName: [{ required: true, message: t('org.cache.formRuleCacheName') }],
    cacheDesc: [{ required: true, message: t('org.cache.formRuleCacheDesc') }],
    cacheExpression: [{ required: true, message: t('org.cache.formRuleCacheExpression') }],
    sortNo: [{ required: true, message: t('org.cache.formRuleSortNo') }],
}

export const cacheIndexPageUrl = "/org/cache/index";
export const cacheAddPageUrl = "/org/cache/add";
export const cacheEditUrl = "/org/cache/edit/";