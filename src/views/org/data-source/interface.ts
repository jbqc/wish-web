/**
 * @author light_dust_generator
 * @since 2021-06-30 11:44:50
 */
import { t } from '/@/i18n/index'

export const dataSourceTableColumns = [
    { title: t('org.data-source.dataSourceCode'), key: "dataSourceCode", dataIndex: "dataSourceCode" },
    { title: t('org.data-source.dataSourceName'), key: "dataSourceName", dataIndex: "dataSourceName", slots: { customRender: "customDataSourceCode" } },
    { title: t('org.data-source.dataSourceDesc'), key: "dataSourceDesc", dataIndex: "dataSourceDesc" },
    { title: t('org.data-source.dataSourceType'), key: "dataSourceType", dataIndex: "dataSourceType" },
    { title: t('org.data-source.status'), key: "status", dataIndex: "status", slots: { customRender: 'customStatus' } },
]

export const dataSourceQueryColumns = [
    { type: "input", name: "likeAll", label: t('org.data-source.likeAll'), },
    { type: "dictionarySelect", name: "dataSourceType", label: t('org.data-source.dataSourceType'), codeItemId: 'ORG_DATASOURCE_TYPE' },
]

export const dataSourceFormRule = {
    dataSourceCode: [{ required: true, message: t('org.data-source.formRuleDataSourceCode') }],
    dataSourceName: [{ required: true, message: t('org.data-source.formRuleDataSourceName') }],
    dataSourceDesc: [{ required: true, message: t('org.data-source.formRuleDataSourceDesc') }],
    dataSourceType: [{ required: true, message: t('org.data-source.formRuleDataSourceType') }],
    userName: [{ required: true, message: t('org.data-source.formRuleUserName') }],
    userPassword: [{ required: true, message: t('org.data-source.formRuleUserPassword') }],
    connUrl: [{ required: true, message: t('org.data-source.formRuleConnUrl') }],
}

export const dataSourceIndexPageUrl = "/org/data-source/index";
export const dataSourceAddPageUrl = "/org/data-source/add";
export const dataSourceEditUrl = "/org/data-source/edit/";