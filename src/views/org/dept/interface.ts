import { t } from '/@/i18n/index'
/**
 * 异步树的配置信息
 */
export const DeptTreeProps = {
    autoExpandParent: {//是否自动展开父节点
        type: Boolean,
        default: true
    },
    showActionIcon: {//是否显示操作按钮图标
        type: Boolean,
        default: false
    },
    //节点前添加 Checkbox 复选框
    checkable: {
        type: Boolean,
        default: false
    },
    //checkable 状态下节点选择完全受控（父子节点选中状态不再关联）
    checkStrictly: {
        type: Boolean,
        default: true
    },
    //checkable 状态下 默认选中的节点
    checkedKeys: {
        type: [Array, Object],
    },
    //checkable 状态下 禁止选中的节点
    disabledKeys: {
        type: Array,
    }
}

export const deptFormRule = {
    deptCode: [{ required: true, message: t('org.dept.formRuleDeptCode') }],
    deptName: [{ required: true, message: t('org.dept.formRuleDeptName') }],
    enabledTime: [{ required: true, message: t('org.dept.formRuleStartTime') }],
}