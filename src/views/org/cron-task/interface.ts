import { t } from '/@/i18n/index'
/** cronTask table columns */
export const cronTaskTableColumns = [
    { title: t('org.cron-task.cronTaskNo'), dataIndex: "cronTaskNo", sorter: true, },
    { title: t('org.cron-task.cronTaskName'), dataIndex: "cronTaskName", slots: { customRender: "customCronTaskName" } },
    { title: t('org.cron-task.active'), dataIndex: "active", slots: { customRender: "customActive" }, align: "center" },
    { title: t('org.cron-task.cronExpression'), dataIndex: "cronExpression" },
    { title: t('org.cron-task.className'), dataIndex: "className" },
    { title: t('org.cron-task.description'), dataIndex: "description" },
    { title: t('org.cron-task.operation'), slots: { customRender: "customOperation" }, dataIndex: "operation", align: "center" }
]
/** cronTask table query columns */
export const cronTaskQueryColumns = []

/** cronTask from rules */
export const cronTaskFormRule = {
    cronTaskName: [{ required: true, trigger: "blur", message: t('org.cron-task.formRuleCronTaskName') }],
    cronTaskNo: [{ type: "number", required: true, trigger: "blur", message: t('org.cron-task.formRuleCronTaskNo') }],
    cronExpression: [{ required: true, trigger: "blur", message: t('org.cron-task.formRuleCronExpression') }],
    className: [{ required: true, trigger: "blur", message: t('org.cron-task.formRuleClassName') }]
}