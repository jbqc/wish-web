import { t } from '/@/i18n/index'
export const userTableColumns = [
    {
        title: t('org.user.userAccount'),
        key: "userAccount",
        dataIndex: "userAccount",
        slots: { customRender: 'customUserAccount' },
        sorter: true,
    },
    { title: t('org.user.userName'), key: "userName", dataIndex: "userName" },
    { title: t('org.user.userPhone'), key: "userPhone", dataIndex: "userPhone" },
    { title: t('org.user.userEmail'), key: "userEmail", dataIndex: "userEmail" },
    {
        title: t('org.user.userSex'),
        dataIndex: "userSex",
        align: "center",
        filters: [
            {
                text: t('common.dictionary.sex.man'),
                value: "MAN",
            },
            {
                text: t('common.dictionary.sex.woman'),
                value: "WOMAN",
            },
        ],
        filterMultiple: false,
        slots: { customRender: "customUserSex" },
        excelKey: 'userSexDesc'
    },
    {
        title: t('org.user.userBrithday'),
        key: "userBrithday",
        dataIndex: "userBrithday",
        hidden: true,
        slots: { customRender: "customUserBrithday" },
    },
];

export const userQueryColumns = [
    { type: "input", name: "userAccount", label: t('org.user.userAccount') },
    { type: "input", name: "userName", label: t('org.user.userName') }
]

export const userFormRule = {
    userAccount: [{ required: true, message: t('org.user.formRuleUserAccount') }, { min: 3, max: 8, message: t('org.user.formRuleUserAccount2'), trigger: "blur" }],
    userName: [{ required: true, message: t('org.user.formRuleUserName') }],
}

export const userIndexPageUrl = "/org/user/index"
export const userAddPageUrl = "/org/user/add";
export const userEditPageUrl = "/org/user/edit/";