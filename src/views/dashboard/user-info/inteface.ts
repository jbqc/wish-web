import { t } from '/@/i18n/index'

//图表数据
export const radarData = [
    {
        item: t('dashboard.user-info.userDetail.design'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 70,
    },
    {
        item: t('dashboard.user-info.userDetail.design'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 30,
    },
    {
        item: t('dashboard.user-info.userDetail.development'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 60,
    },
    {
        item: t('dashboard.user-info.userDetail.development'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 70,
    },
    {
        item: t('dashboard.user-info.userDetail.marketing'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 50,
    },
    {
        item: t('dashboard.user-info.userDetail.marketing'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 60,
    },
    {
        item: t('dashboard.user-info.userDetail.test'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 60,
    },
    {
        item: t('dashboard.user-info.userDetail.test'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 70,
    },
    {
        item: t('dashboard.user-info.userDetail.language'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 70,
    },
    {
        item: t('dashboard.user-info.userDetail.language'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 50,
    },
    {
        item: t('dashboard.user-info.userDetail.technology'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 50,
    },
    {
        item: t('dashboard.user-info.userDetail.technology'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 40,
    },
    {
        item: t('dashboard.user-info.userDetail.support'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 30,
    },
    {
        item: t('dashboard.user-info.userDetail.support'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 40,
    },
    {
        item: t('dashboard.user-info.userDetail.sales'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 60,
    },
    {
        item: t('dashboard.user-info.userDetail.sales'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 40,
    },
    {
        item: t('dashboard.user-info.userDetail.ux'),
        user: t('dashboard.user-info.userDetail.personal'),
        score: 50,
    },
    {
        item: t('dashboard.user-info.userDetail.ux'),
        user: t('dashboard.user-info.userDetail.team'),
        score: 60,
    },
]