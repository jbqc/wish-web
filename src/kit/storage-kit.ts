
/**
 * 得到当前的缓存对象
 */
function getStorage(): Storage {
    return sessionStorage;
}
/**
 * 设置缓存(与localStorage sessionStorage)方法一致
 * @param key 缓存key
 * @param value 缓存值
 */
export function setItem(key: string, value: string) {
    getStorage().setItem(key, value)
}

/**
 * 获取缓存(与localStorage sessionStorage)方法一致
 * @param key 缓存key
 */
export function getItem(key: string): string {
    return getStorage().getItem(key)
}

/**
 * 删除缓存(与localStorage sessionStorage)方法一致
 * @param key 缓存key
 */
export function removeItem(key: string) {
    getStorage().removeItem(key)
}

/**
 * 清理所有缓存
 */
export function clear() {
    getStorage().clear();
}