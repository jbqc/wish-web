import { getItem, setItem, removeItem } from '/@/kit/storage-kit'
/**
 * 定义token key
 */
export const TOKEN_KEY = "Access-Token";

/**
 * 获取token
 * @author zhu_liangyu
 */
export function getAccessToken(): string {
    return getItem(TOKEN_KEY);
}

/**
 * 设置token
 * @param token token
 * @author zhu_liangyu
 */
export function setAccessToken(token: string): void {
    setItem(TOKEN_KEY, token);
}

/**
 * 清理token
 * @author zhu_liangyu
 */
export function clearAccessToken(): void {
    removeItem(TOKEN_KEY);
}