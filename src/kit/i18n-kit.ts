import { i18nCNKey } from "../framework/profile";
import { getItem, setItem } from "./storage-kit";

export const locale_storage_key = 'WISH_USER_LOACLE';

export function getLocale(): string {
    return getItem(locale_storage_key) || i18nCNKey;
}

export function setLocale(_locale: string) {
    setItem(locale_storage_key, _locale);
}