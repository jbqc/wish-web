/**
 * 判断对象是否非空
 * @param obj 
 */
export function isBlank(obj: any): boolean {
    return obj === null || obj === undefined;
}

/**
 * 判断对象是否非空
 * @param obj 
 */
export function notBlank(obj: any): boolean {
    return !isBlank(obj);
}

/**
 * 判断字符串是否为空
 * @param str 
 */
export function isBlankForString(str: string): boolean {
    return str === null || str === undefined || str === "";
}

/**
 * 判断字符串是否非空
 * @param str 
 */
export function notBlankForString(str: string): boolean {
    return !isBlankForString(str);
}

/**
 * 判断数组对象是否为空
 * @param array 数组
 */
export function isBlankForArray(array: Array<any>): boolean {
    return array === null || array === undefined || array.length == 0;
}

/**
 * 判断数组对象是否非空
 * @param array 数组
 */
export function notBlankForArray(array: Array<any>): boolean {
    return !isBlankForArray(array);
}

/**
 *判断map是否为空
 */
export function isBlankForMap(map: Map<any, any>): boolean {
    return Object.keys(map).length === 0
}

/**
 *判断map是否非空
 */
export function notBlankForMap(map: Map<any, any>): boolean {
    return !isBlankForMap(map);
}