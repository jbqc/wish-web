/**
 * 数组删除指定元素
 * @param arr 数组
 * @param removeStr 被删除的字符串
 * @returns 
 */
export function removeStr(arr: Array<string>, removeStr: string) {
    var array = new Array();
    arr.forEach(item => {
        if (item.indexOf(removeStr) == -1) array.push(item)
    })
    return array;
}