
import { RouteRecordRaw } from "vue-router";
import { notBlankForArray } from "./blank-kit";
import AsyncRoutes from "/@/router/routes/index";

/**
 * 获取需要渲染菜单的路由
 * @param userRoutes 
 */
export const getMenuRoutes = (userRoutes: Array<string>): Array<RouteRecordRaw> => {
    return filterMenuRoutes(JSON.parse(JSON.stringify(AsyncRoutes)), userRoutes, "");
}

/**
 * 过滤菜单路由 如果用户不包含某个路由 则删除其 使菜单渲染不出现该菜单
 * @param asyncRoutes 路由配置中的所有动态路由
 * @param userRoutes 用户有的路由路径数组
 */
function filterMenuRoutes(asyncRoutes: Array<RouteRecordRaw>, userRoutes: Array<string>, basePath: string): Array<RouteRecordRaw> {
    // for (var i = 0; i < asyncRoutes.length; i++) {
    //     let _childPath = basePath + (asyncRoutes[i].path.indexOf("/") == 0 ? asyncRoutes[i].path : "/" + asyncRoutes[i].path);
    //     if (userRoutes.indexOf(_childPath) == -1) {
    //         asyncRoutes.splice(i, 1); // 将使后面的元素依次前移，数组长度减1
    //         i--; // 如果不减，将漏掉一个元素
    //         continue;
    //     }
    //     if (notBlankForArray(asyncRoutes[i].children)) {
    //         asyncRoutes[i].children = filterMenuRoutes(asyncRoutes[i].children, userRoutes, _childPath)
    //     }
    // }
    for (var i = 0; i < asyncRoutes.length; i++) {
        if (userRoutes.indexOf(asyncRoutes[i].meta.menuId as string) == -1) {
            asyncRoutes.splice(i, 1); // 将使后面的元素依次前移，数组长度减1
            i--; // 如果不减，将漏掉一个元素
            continue;
        }
        if (notBlankForArray(asyncRoutes[i].children)) {
            let _childPath = basePath + (asyncRoutes[i].path.indexOf("/") == 0 ? asyncRoutes[i].path : "/" + asyncRoutes[i].path);
            asyncRoutes[i].children = filterMenuRoutes(asyncRoutes[i].children, userRoutes, _childPath)
        }
    }
    return asyncRoutes;
}