import { WishAntMenu, WishAntMenuObj, WishAntMenuTree } from "/@/_types/menu";
import { notBlankForArray } from "./blank-kit";
import { RouteRecordRaw } from "vue-router";

export function routeCaseToAntMenu(routeArray: Array<RouteRecordRaw>): WishAntMenuObj {
    /**
     * hiddenMenuKey:[] //哪些菜单路由被隐藏 隐藏路由在返回时会被删除掉
     * rootSubmenuKeys:[]//子菜单的根菜单
     * menus:[]
     */
    const wishAntMenuObj: WishAntMenuObj = { hiddenMenuKeys: [], rootSubmenuKeys: routeArray.map(item => item.path), menus: [], allKes: [] };
    wishAntMenuObj.menus = routeCaseToWishAntMenu(routeArray, "", wishAntMenuObj.allKes, wishAntMenuObj.hiddenMenuKeys);
    return wishAntMenuObj;
}

/**
 * wishMenu 转换为 wishAntMenu
 * @param routeArray 后台菜单数据
 * @param basePath 根path
 * @param hiddenMenuKeys 隐藏菜单key
 * @param allKes 所有key
 */
function routeCaseToWishAntMenu(routeArray: Array<RouteRecordRaw>, basePath: string, allKes: Array<string>, hiddenMenuKeys: Array<string>): Array<WishAntMenu> {
    const wishAntMenuArray: Array<WishAntMenu> = [];
    for (let i in routeArray) {
        let _childPath = routeArray[i].path.indexOf("/") == 0 ? routeArray[i].path : "/" + routeArray[i].path;
        let path = basePath + _childPath;
        //如果菜单是隐藏的 我们不再去管其子菜单 
        if (routeArray[i].meta.hidden === true) {
            hiddenMenuKeys.push(path)
            continue;
        }
        allKes.push(path);
        const wishAntMenu: WishAntMenu = { key: path, path, title: routeArray[i].meta.title as string, icon: routeArray[i].meta.icon as string };
        if (notBlankForArray(routeArray[i].children)) {
            //判断其子菜单是否有显示的，如果有显示则进行自己遍历 否则不进行任何处理
            let childNotHiddenMenus = routeArray[i].children.filter(item => {
                return item.meta.hidden !== true;
            })
            if (notBlankForArray(childNotHiddenMenus)) {
                wishAntMenu.children = routeCaseToWishAntMenu(routeArray[i].children, path, allKes, hiddenMenuKeys);
            }
        }
        wishAntMenuArray.push(wishAntMenu);
    }
    return wishAntMenuArray;
}

/**
 * 将本地路由转换为支持antdvue的tree
 * @param routeArray 
 */
export function getMenuTree(routeArray: Array<RouteRecordRaw>): Array<WishAntMenuTree> {
    return routeCaseToWishAntMenuTree(routeArray, "");
}

function routeCaseToWishAntMenuTree(routeArray: Array<RouteRecordRaw>, basePath: string): Array<WishAntMenuTree> {
    const wishAntMenuTreeArray = [];
    for (let i in routeArray) {
        let _childPath = routeArray[i].path.indexOf("/") == 0 ? routeArray[i].path : "/" + routeArray[i].path;
        let path = basePath + _childPath;
        const wishAntMenuTree: WishAntMenuTree = { menuId: routeArray[i].meta.menuId as string, key: path, path, title: routeArray[i].meta.title as string, icon: routeArray[i].meta.icon as string, hidden: routeArray[i].meta.hidden === true };
        if (notBlankForArray(routeArray[i].children)) {
            //判断其子菜单是否有显示的，如果有显示则进行自己遍历 否则不进行任何处理
            let childNotHiddenMenus = routeArray[i].children.filter(item => {
                return item.meta.hidden !== true;
            })
            if (notBlankForArray(childNotHiddenMenus)) {
                wishAntMenuTree.children = routeCaseToWishAntMenuTree(routeArray[i].children, path);
            }
        }
        wishAntMenuTreeArray.push(wishAntMenuTree);
    }
    return wishAntMenuTreeArray;
}