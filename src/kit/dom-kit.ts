/**
 * 禁用某一区块的所有dom元素
 * @param divDom div元素
 */
function disabledDivDom(divDom: HTMLElement) {
    divDom.classList.add("wish-disabled-div")
    //为了防止还能使用tab键选中 我们需要在当前禁用div内增加一个fieldset标签 使其包裹内容
    divDom.querySelectorAll("fieldset").forEach((item: HTMLElement) => {
        item.setAttribute("disabled", "true");
    })
    //设置ant input框禁用
    divDom.querySelectorAll(".ant-input").forEach((item: HTMLElement) => {
        item.classList.add("ant-input-disabled")
    })
    //设置ant select框禁用
    divDom.querySelectorAll(".ant-select").forEach((item: HTMLElement) => {
        item.classList.add("ant-select-disabled")
    })
    //设置ant radio框禁用
    divDom.querySelectorAll(".ant-radio-group").forEach((item: HTMLElement) => {
        item.querySelectorAll("label").forEach((labelItem: HTMLElement) => {
            labelItem.classList.add("ant-radio-wrapper-disabled")
            let spanItem = labelItem.querySelector("span");
            if (spanItem) {
                spanItem.classList.add("ant-radio-disabled");
                let inputItem = spanItem.querySelector("input");
                if (inputItem) inputItem.setAttribute("disabled", "true");
            }
        })
    })
    //设置ant rate框禁用
    divDom.querySelectorAll(".ant-rate").forEach((item: HTMLElement) => {
        item.classList.add("ant-rate-disabled")
    })
    //设置ant checkbox框禁用
    divDom.querySelectorAll(".ant-checkbox-wrapper").forEach((item: HTMLElement) => {
        item.classList.add("ant-checkbox-wrapper-disabled")
    })
    divDom.querySelectorAll(".ant-checkbox").forEach((item: HTMLElement) => {
        item.classList.add("ant-checkbox-disabled")
    })
    //设置ant switch框禁用
    divDom.querySelectorAll(".ant-switch").forEach((item: HTMLElement) => {
        item.classList.add("ant-switch-disabled")
    })
}

/**
 * 解除禁用某一区块的所有dom元素
 * @param divDom div元素
 */
function removeDisabledDivDom(divDom: HTMLElement) {
    divDom.classList.remove("wish-disabled-div")
    divDom.querySelectorAll("fieldset").forEach((item: HTMLElement) => {
        item.removeAttribute("disabled")
    })
    //解除ant input框禁用
    divDom.querySelectorAll(".ant-input").forEach((item: HTMLElement) => {
        item.classList.remove("ant-input-disabled")
    })
    //解除ant select框禁用
    divDom.querySelectorAll(".ant-select").forEach((item: HTMLElement) => {
        item.classList.remove("ant-select-disabled")
    })
    //解除ant radio框禁用
    divDom.querySelectorAll(".ant-radio-group").forEach((item: HTMLElement) => {
        item.querySelectorAll("label").forEach((labelItem: HTMLElement) => {
            labelItem.classList.remove("ant-radio-wrapper-disabled")
            let spanItem = labelItem.querySelector("span");
            if (spanItem) {
                spanItem.classList.remove("ant-radio-disabled");
                let inputItem = spanItem.querySelector("input");
                if (inputItem) inputItem.removeAttribute("disabled");
            }
        })
    })
    //解除ant rate框禁用
    divDom.querySelectorAll(".ant-rate").forEach((item: HTMLElement) => {
        item.classList.remove("ant-rate-disabled")
    })
    //解除ant checkbox框禁用
    divDom.querySelectorAll(".ant-checkbox-wrapper").forEach((item: HTMLElement) => {
        item.classList.remove("ant-checkbox-wrapper-disabled")
    })
    divDom.querySelectorAll(".ant-checkbox").forEach((item: HTMLElement) => {
        item.classList.remove("ant-checkbox-disabled")
    })
    //解除ant switch框禁用
    divDom.querySelectorAll(".ant-switch").forEach((item: HTMLElement) => {
        item.classList.remove("ant-switch-disabled")
    })
}

/**
 * 禁用某个div不可选中
 * @param divId 需要禁用的div的id
 */
export function disabledDivDomById(divId: string) {
    disabledDivDom(document.getElementById(divId));
}

/**
 * 禁用某个class所有的div不可选中 (未测试)
 * @param className 需要禁用的div的className
 */
export function disabledDivDomByClass(className: string) {
    document.querySelectorAll("." + className).forEach((item: HTMLElement) => disabledDivDom(item))
}


/**
 * 禁用某个div不可选中
 * @param divId 需要解除禁用的div的id
 */
export function removeDisabledDivDomById(divId: string) {
    removeDisabledDivDom(document.getElementById(divId));
}

/**
 * 解除禁用某个class所有的div不可选中 (未测试)
 * @param className 需要解除禁用的div的className
 */
export function removeDisabledDivDomByClass(className: string) {
    document.querySelectorAll("." + className).forEach((item: HTMLElement) => removeDisabledDivDom(item))
}