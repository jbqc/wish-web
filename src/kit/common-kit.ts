import { RouteLocationRaw } from "vue-router"
import router from "/@/router"
/**
 * 返回首页
 */
export const backHome = () => {
    router.push({ path: '/' })
}

/**
 * 去某个组件
 * @param to RouteLocationRaw对象
 */
export const goTo = (to: RouteLocationRaw) => {
    router.push(to);
}

/**
 * 跳转去某个组件
 * @param path 路径
 */
export const redirectTo = (path: string) => {
    router.replace({ path: "/redirect" + path });
}

/**
 * 刷新当前组件
 * @param path 路径
 */
export const reload = () => {
    router.replace({
        path: "/redirect" + router.currentRoute.value.fullPath,
    });
}



/**
 * 将url参数转换为对象
 * @param url 
 */
export function urlToMap(url: string) {
    var params = url.split("?")[1].split("&"), obj = {};
    params.map(item => obj[item.split("=")[0]] = item.split("=")[1])
    return obj;
}

/**
 * 
 * @param url 将url转换为vueRouter
 */
export function urlToVueRouter(url: string) {
    var arr = url.split("?");
    return { path: arr[0], query: urlToMap(url) }
}

/**
 * 字符串全部替换
 * @param str 
 */
export function replaceAllForString(str: string, serachValue: string, replaceValue: string): string {
    while (str.indexOf(serachValue) != -1) {
        str.replace(serachValue, replaceValue);
    }
    return str;
}


/**
 * 字符串全部替换
 * @param str 被替换的字符串
 * @param serachValue 正则表达式
 * @param replaceValue 替换后的值
 * @returns 
 */
export function replaceAllForStringByRegExp(str: string, serachValue: RegExp, replaceValue: string): string {
    return str.replace(serachValue, replaceValue);

}

/**
 * @description: 
 * @param {config} 
 * @return: 
 * @author: Zhu_liangyu
 */
export function downloadFile(config: any) {
    const blob = config.blob;
    const elink = document.createElement("a");
    elink.download = config.fileName;
    elink.style.display = "none";
    elink.href = URL.createObjectURL(blob);
    document.body.appendChild(elink);
    elink.click();
    URL.revokeObjectURL(elink.href); // 释放URL 对象
    document.body.removeChild(elink);
}