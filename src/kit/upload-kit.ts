import { message } from "ant-design-vue";
import { t } from '/@/i18n/index'
import { FileInfo, FileItem, UploadChangeConfig } from "/@/_types/file";

export const imageAccept = "image/jpeg,image/png";
export const imageMaxSize = 2 * 1024 * 1024;
/**
 * 上传图片前判断类型与大小是否允许
 */
export function beforeImage(file: FileItem) {
    const isAllowAccept = imageAccept.indexOf(file.type) != -1;
    const isAllowSize = file.size < imageMaxSize;
    if (!isAllowAccept) {
        message.error(t('kit.upload.onlyAccept', { 'accept': 'JPG' }));
    }
    if (!isAllowSize) {
        message.error(t('kit.upload.onlyAccept', { 'smallerSize': '2MB' }));
    }
    return isAllowAccept && isAllowSize;
}


export function antUploadChange(fileInfo: FileInfo, config: UploadChangeConfig) {

    if (fileInfo.file.status === "uploading") {
        if (config.uploading) config.uploading();
        return;
    }
    if (fileInfo.file.status === "done") {
        console.log(fileInfo)
        if (config.done) config.done();
        return;
    }
    if (fileInfo.file.status === "error") {
        message.error("upload error");
        if (config.error) config.error();
        return;
    }
}