# wish-web

#### 介绍
ant-design-vue2.x typescript vue3.x vite 集成的一个小平台

#### 软件架构
Typescript4.x  vite2.x vue3.x ant-design-vue2.x(UI框架) axios(请求) g2plot(图表库)

##### 首页展示

<img src="./static/analysis.jpg" width="100%" height="100%"/>  

##### 后端代码

[wish-server](https://gitee.com/jbqc/wish-server)

##### 演示环境

[https://web.jiubanqingchen.cn/](https://web.jiubanqingchen.cn/)  demo 123456

##### 功能模块

- 用户管理
- 角色管理
- 部门管理
- 菜单管理
- 数据字典管理
- 定时任务
- 国际化
- 登录日志
- 代码生成(这个功能独立出去了一个项目 [wish-code-tool-server](https://gitee.com/jbqc/wish-code-tool-server))


#### 安装教程

```bash
#直接下载项目
git clone https://gitee.com/jbqc/wish-web.git

#安装前端依赖文件 类似后端的mavne
npm install

#运行项目
npm run dev

#打包代码
npm run build
```

#### 目录说明

```
├── /src/            # 源码目录
│ ├── /_types/       # 类型定义文件
│ ├── /api/          # 请求接口文件
│ ├── /assets/       # 组件静态资源(图片)
│ ├── /components/   # 公共组件
│ ├── /framework/    # 框架配置
│ ├── /i18n/         # 国际化文件
│ ├── /kit/          # 一些工具类的存放
│ ├── /layouts/      # 文件布局
│ ├── /router/       # 路由配置
│ ├── /store/        # vuex状态管理
│ ├── /styles/       # 样式文件
│ ├── /views/        # 路由组件（页面文件）
│ ├── App.vue        # 组件入口
│ └── main.js        # 程序入口
├── .gitignore       # git忽略文件
├── index.html       # 页面入口
├── package.json     # 项目依赖
├── README.md        # 项目文档
├── tsconfig.json    # ts配置文件
└── vite.config.ts   # vite的配置文件
```

#### 项目截图

- 用户管理

<img src="./static/user-index.jpg" width="100%" height="100%"/>

<img src="./static/user-from.jpg" width="100%" height="100%"/>

<img src="./static/user-index-en.jpg" width="100%" height="100%"/>

- 角色管理

<img src="./static/role-index.jpg" width="100%" height="100%"/>

<img src="./static/user-from.jpg" width="100%" height="100%"/>

- 定时任务

<img src="./static/cron-task.jpg" width="100%" height="100%"/>


#### 使用说明

这里和wish_server中同步 新增一个demo页面出来

- Api(请求配置文件) demo.ts文件，我在modelApi中配置了增删改查得方法 和后台是对应得

```javascript
/**
 * @author light_dust_generator
 * @since 2021-06-25 11:39:25
 */
import { ModelApi } from "/@/_types/api";

class DemoApi extends ModelApi {
    //...这里可以重写父类方法,也可以增加新的方法
}
const demoApi = new DemoApi("/org/demo", "demoId");

export default demoApi;
```

- 国际化文件demo.ts，国际化翻译文件(后期同步到数据库中)

```javascript
/**
 * @author light_dust_generator
 * @since 2021-06-25 11:39:25
 */
export default {
    demoTableTitle: '演示表格',
    demoId:'主键',
    demoName:'演示名称',
    demoDesc:'演示描述',
    demoDate:'演示时间',
    demoPerson:'演示人',
    //提示
    formPlhDemoId:'请输入主键',
    formPlhDemoName:'请输入演示名称',
    formPlhDemoDesc:'请输入演示描述',
    formPlhDemoDate:'请输入演示时间',
    formPlhDemoPerson:'请输入演示人',
    //校验
    formRuleDemoId:'主键为必填项',
    //表单头部
    addDemoTitle: '新建演示',
    editDemoTitle: '编辑演示'
}
```

- 配置文件indexface.ts 这个是用来配置表格字段，表格查询，表单字段、提示、必填规则等的配置文件

```typescript
/**
 * @author light_dust_generator
 * @since 2021-06-25 11:39:25
 */
import { t } from '/@/i18n/index'

export const demoTableColumns =[
    { title: t('org.demo.demoId'), key: "demoId", dataIndex: "demoId"},
    { title: t('org.demo.demoName'), key: "demoName", dataIndex: "demoName",slots: { customRender: "customDemoId" } },
    { title: t('org.demo.demoDesc'), key: "demoDesc", dataIndex: "demoDesc"},
    { title: t('org.demo.demoDate'), key: "demoDate", dataIndex: "demoDate"},
    { title: t('org.demo.demoPerson'), key: "demoPerson", dataIndex: "demoPerson"},
]

//查询字段
export const demoQueryColumns = [
]

//表单必填规则
export const demoFormRule = {
    demoId: [{ required: true, message: t('org.demo.formRuleDemoId') }],
}

//这个对应路由
export const demoIndexPageUrl = "/org/demo/list";
export const demoAddPageUrl = "/org/demo/add";
export const demoEditUrl = "/org/demo/edit/";
```

上面是三个辅助文件  其实也可以写在页面之中 但是为了方便扩展所以分割了出来

对于页面 又做了一个拆分 建demoList demoForm单独拆分了出来作为组件 在用index 和form分别引用他们 这样做的好处是我在其他页面嵌入demoList不用将列表页面的所有元素嵌入过去 一定程度上方便的多次在不同的环境中引用，表单也是一样

```
├── /demo/            		# demo文件夹
│ ├── /component/     		# 类型定义文件
| |── |── demoList.vue		列表组件
| |── |── demoForm.vue		表单组件
│ ├── index.vue       		# 路由上的列表页
│ ├── from.vue        		#路由上的表单页
```

下面上代码

- demoList.vue(列表组件)



```vue
<template>
  <qc-page-table ref="demoPageTableRef" :columns="columns"  :loadData="loadData" :rowKey="(record) => record[primaryKey]">
    <template #toolbarTitle>
      {{ t("org.demo.demoTableTitle") }}
    </template>
    <template #toolbarButton>
      <qc-add-button @click="addButtonClick"></qc-add-button>
      <qc-delete-button @click="deleteButtonClick"></qc-delete-button>
    </template>
    <template #customDemoId="{ text, record }">
      <qc-link :url="demoEditUrl + record[primaryKey]">
        {{ text }}
      </qc-link>
    </template>
  </qc-page-table>
</template>
<script lang='ts'>
import { defineComponent,onMounted,  reactive, ref, toRefs } from "vue";
import { useI18n } from "vue-i18n";
import { useRouter } from "vue-router";
import demoApi from "/@/api/org/demo";
import {demoTableColumns,demoQueryColumns,demoAddPageUrl,demoEditUrl} from "../interface";
import { useModelPageTable } from "/@/kit/model-kit";
export default defineComponent({
  name: "DemoList",
  setup(props) {
    const { t } = useI18n();
    const router = useRouter();
    const demoPageTableRef = ref();
    //这个是自己定义的一个表格方法 之前是直接对表格做封装太笼统了 现在使用这种api的方式使得更容易拆分 好处自己体会吧
    const {primaryKey,searchTable,reloadTable,loadData,deleteSelectData} = useModelPageTable(demoPageTableRef, demoApi, );
    const demoState = reactive({
      demoEditUrl,
      columns: demoTableColumns,
      primaryKey,
      loadData,
      addButtonClick: () => router.push({ path: demoAddPageUrl }),
      deleteButtonClick: () => deleteSelectData(),
    });
    onMounted(() => searchTable());
    return {
      t,
      ...toRefs(demoState),
      demoPageTableRef,
      searchTable,
    };
  },
});
</script>
```

- demoForm.vue(你会发现表单页面没有保存按钮 是的 并没有 但是他会提供一个onSubmit方法 谁要保存谁就调用这个方法)

```vue
<template>
  <a-form ref="demoFormRef" :model="demoModel" :rules="demoFormRule" v-bind="layout">
    <a-form-item name="demoId" :label="t('org.demo.demoId')">
        <a-input v-model:value="demoModel.demoId" :placeholder="t('org.demo.formPlhDemoId')" />
    </a-form-item>
    <a-form-item name="demoName" :label="t('org.demo.demoName')">
        <a-input v-model:value="demoModel.demoName" :placeholder="t('org.demo.formPlhDemoName')" />
    </a-form-item>
    <a-form-item name="demoDesc" :label="t('org.demo.demoDesc')">
        <a-input v-model:value="demoModel.demoDesc" :placeholder="t('org.demo.formPlhDemoDesc')" />
    </a-form-item>
    <a-form-item name="demoDate" :label="t('org.demo.demoDate')">
        <a-input v-model:value="demoModel.demoDate" :placeholder="t('org.demo.formPlhDemoDate')" />
    </a-form-item>
    <a-form-item name="demoPerson" :label="t('org.demo.demoPerson')">
        <a-input v-model:value="demoModel.demoPerson" :placeholder="t('org.demo.formPlhDemoPerson')" />
    </a-form-item>
  </a-form>
</template>
<script lang='ts'>
import { defineComponent, ref } from "vue";
import demoApi from "/@/api/org/demo";
import { demoFormRule,demoEditUrl } from "../interface";
import { useModelForm } from "/@/kit/model-kit";
import { useI18n } from "vue-i18n";
export default defineComponent({
  props: {
   demoId: String,
  },
  setup(props, { emit }) {
    const { t } = useI18n();
    const demoFormRef = ref();
    const demoModel = ref({});

    const { save } = useModelForm(demoFormRef, demoModel, props.demoId, demoApi)
        //提交方法
    const onSubmit = () => save().then((res) => emit('submitSuccess', res.data.data))
    const layout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 15 },
    };
    return {
      t,
      demoFormRef,
      demoModel,
      demoFormRule,
      layout,
      onSubmit,
    };
  },
});
</script>
```

- index.vue(这个页面引用了demoList.vue组件)

```vue
/**
 * @author light_dust_generator
 * @since 2021-06-25 11:39:25
 */
<template>
  <qc-page-header></qc-page-header>
  <demo-table ref="demoRef"></demo-table>
</template>
<script lang='ts'>
import { defineComponent,ref } from "vue";
import DemoTable from "./component/demo-list.vue";
export default defineComponent({
  components: {DemoTable},
  setup() {
        const demoRef=ref();
        return {demoRef}
  },
});
</script>
```

- form.vue(这个页面引用了demoForm.vue 你会发现这里面有保存按钮 )

```vue
/**
 * @author light_dust_generator
 * @since 2021-06-25 11:39:25
 */
 <template>
   <qc-page-header @back="goBack"></qc-page-header>
   <a-card :bordered="false" :title="t('org.demo.demoCardTitle')">
     <template #extra>
       <qc-save-button @click="save"></qc-save-button>
     </template>
     <demo-form ref="demoFormRef" :demoId="demoId"></demo-form>
   </a-card>
 </template>
 <script lang='ts'>
 import { computed, defineComponent, reactive, ref, toRefs } from "vue";
 import { useRouter } from "vue-router";
 import { useI18n } from "vue-i18n";
 import { demoIndexPageUrl } from "./interface";
 import DemoForm from "./component/demo-form.vue";
 export default defineComponent({
   components: {DemoForm},
   setup() {
     const { t } = useI18n();
     const demoFormRef = ref();
     const router = useRouter();
     const formState = reactive({
       demoId: router.currentRoute.value.params.id,
       save: () => demoFormRef.value.onSubmit(),
       goBack: () => router.push({ path: demoIndexPageUrl }),
     });
     return {
       t,
       demoFormRef,
       ...toRefs(formState),
     };
   },
 });
 </script>
```



#### 参与贡献



1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
这里输入代码
