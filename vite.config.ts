import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { resolve } from 'path'

function pathResolve(dir: string) {
    return resolve(__dirname, ".", dir);
}


export default defineConfig({
    define: {
        __VUE_I18N_LEGACY_API__: false,
        __VUE_I18N_FULL_INSTALL__: false,
        __INTLIFY_PROD_DEVTOOLS__: false,
    },
    plugins: [
        vue(),
        vueJsx(),
    ],
    base: "/",
    resolve: {
        alias: {
            '/@': pathResolve('./src'),
            'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js' //解决警告You are running the esm-bundler build of vue-i18n. It is recommended to configure your bundler to explicitly replace feature flag globals with boolean literals to get proper tree-shaking in the final bundle.
        },
    },
    optimizeDeps: {
        include: [
            "@ant-design/icons-vue",
            'ant-design-vue/es/locale/zh_CN',
            'ant-design-vue/es/locale/en_US'
        ]
    },
    server: {
        port: 3334,
        proxy: {
            '/api': {
                target: 'http://localhost:80/',  // 后台接口域名
                //ws: true,        //如果要代理 websockets，配置这个参数
                //secure: false,  // 如果是https接口，需要配置这个参数
                changeOrigin: true,  //是否跨域
                rewrite: (path) => path.replace(/^\/api/, '/wish')
            }

        }
    },
    build: {
        brotliSize: false,
        rollupOptions: {
            // output: {
            //     manualChunks(id) {
            //         if (id.includes('node_modules')) {
            //             return id.toString().split('node_modules/')[1].split('/')[0].toString();
            //         }
            //     }
            // }
        }
    }
})